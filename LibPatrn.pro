#-------------------------------------------------
#
# Project created by QtCreator 2014-02-24T19:41:59
#
#-------------------------------------------------

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = patrn
TEMPLATE = lib
CONFIG += staticlib

SOURCES += libpatrn.cpp \
    NvmStorage.cpp \
    PatrnFiles.cpp \
    RpiJson.cpp

HEADERS += libpatrn.h \
    NvmStorage.h \
    PatrnFiles.h \
    RpiJson.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

DEFINES += LIBPATRN_LIBRARY
