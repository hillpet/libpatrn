/*  (c) Copyright:  2014...2021 Patrn ESS
**
**  $Workfile:  NvmStorage.cpp $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Implementation of the NVM permanent storage class
**
**  Entries:
**
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  24 Feb 2014         Created
 *  24 Feb 2021         Add Put/Get double
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <QFile>
#include "NvmStorage.h"

int         CNvmStorage::iInstance;
PQSTR      *CNvmStorage::ppclNVstore;
int         CNvmStorage::iNumWnvms;
int         CNvmStorage::iNumCallbacks;
TFunctor  **CNvmStorage::ppfunCallbacks;

//
// This is the storage class for Non volatile data in any form. All data is serialized 
// and stored into a QString array in ASCII format.
//
//  To store/retrieve :
//
//  Qstring data:       NvmPut(ID, clString)
//                      NvmGet(ID, &clString)
//  int:                NvmPut(ID, &iNr)
//  ASCIIz              NvmPut(ID, pzChar)
//                      NvmGet(ID, pzChar) NOTE: pzChar MUST be preformatted to hold the data (with non-zero data).
//  Array of quint8:    NvmPut(ID, pubData, size)
//                      NvmGet(ID, pubData, size)
//

//
//  Function:  CNvmStorage
//  Purpose:   Constructor
//
//  Parms:     
//  Returns:   
//  Note:      Somebody needs to call the constructor with the actual number of members in the archive.
//
CNvmStorage::CNvmStorage(QWidget *pclParent) : QWidget(pclParent)
{
   iInstance++;
}

//
//  Function:  CNvmStorage
//  Purpose:   Constructor
//
//  Parms:     Number of archive members
//  Returns:   
//  Note:      Somebody needs to call the constructor with the actual number of members in the archive.
//
CNvmStorage::CNvmStorage(QWidget *pclParent, int iNum) : QWidget(pclParent)
{
   iInstance++;
   //
   if(iNumWnvms == 0)
   {
      iNumCallbacks  = 0;
      ppfunCallbacks = nullptr;
      //
      iNumWnvms = iNum;
      //
      // C++: No Variable Length Arrays: 
      //       clNVstore = new(QString[iNum]);
      //
      ppclNVstore = new PQSTR[iNum];
      //
      // Fill the array with QStrings
      //
      for(int i=0; i<iNum; ++i)
      {
         ppclNVstore[i] = new(QString);
      }
   }
}

//
//  Function:  CNvmStorage
//  Purpose:   Constructor
//
//  Parms:     
//  Returns:   
//  Note:      Somenody needs to call the constructor with the actual number of members in the archive.
//
CNvmStorage::CNvmStorage()
{
   iInstance++;
}

//
//  Function:  CNvmStorage
//  Purpose:   Constructor
//
//  Parms:     Number of archive members
//  Returns:   
//  Note:      Somenody needs to call the constructor with the actual number of members in the archive.
//
CNvmStorage::CNvmStorage(int iNum)
{
   iInstance++;
   //
   if(iNumWnvms == 0)
   {
      iNumCallbacks  = 0;
      ppfunCallbacks = nullptr;
      //
      iNumWnvms = iNum;
      //
      // C++: No Variable Length Arrays: 
      //       clNVstore = new(QString[iNum]);
      //
      ppclNVstore = new PQSTR[iNum];
      //
      // Fill the array with QStrings
      //
      for(int i=0; i<iNum; ++i)
      {
         ppclNVstore[i] = new(QString);
      }
   }
}

//
//  Function:  ~CNvmStorage
//  Purpose:   Destructor
//
//  Parms:     
//  Returns:   
//
CNvmStorage::~CNvmStorage()
{
   if(--iInstance == 0)
   {
      for(int i=0; i<iNumWnvms; ++i)
      {
         delete(ppclNVstore[i]);
      }
   }
}

//
//  Function:  NvmSetCallbacks
//  Purpose:   Setup the Nvm-changed callback functions
//
//  Parms:     List of callbacks, nr of callbacks
//  Returns:   
//
void CNvmStorage::NvmSetCallbacks(TFunctor **ppfunCbs, int iNum)
{
   iNumCallbacks  = iNum;
   ppfunCallbacks = ppfunCbs;
}

//
//  Function:  NvmSetCallbacks
//  Purpose:   Setup the Nvm-changed callback functions
//
//  Parms:     List of callbacks (nullptr terminated)
//  Returns:   
//
void CNvmStorage::NvmSetCallbacks(TFunctor **ppfunCbs)
{
   TFunctor   *pfunTmp;

   iNumCallbacks  = 0;
   ppfunCallbacks = ppfunCbs;
   //
   do
   {
      pfunTmp = ppfunCbs[iNumCallbacks++];
   }
   while(pfunTmp);
}

//
//  Function:  NvmHasChanged
//  Purpose:   
//
//  Parms:     
//  Returns:   
//  Note:      Virtual function
//
void CNvmStorage::NvmHasChanged(int)
{
}

//
//  Function:  NvmRead
//  Purpose:   Read the data file into the NVM
//
//  Parms:     File pathname
//  Returns:   true if OKee opened
//
bool CNvmStorage::NvmRead(QString &clFilename)
{
   QByteArray baTemp = clFilename.toUtf8();
   char *pcFilename  = baTemp.data();

   return( NvmRead(pcFilename) );

}

//
//  Function:  NvmRead
//  Purpose:   Read the data file into the NVM
//
//  Parms:     File pathname
//  Returns:   true if OKee opened
//
bool CNvmStorage::NvmRead(char *pcFilename)
{
   bool     fCC=false;
   int      iValue, iNvmNr=1;

   QFile clFile(pcFilename);
   if( clFile.open(QIODevice::ReadOnly) )
   {
      //
      // Read the data serialized from the file
      // The first element in every archive is the number of NVM members
      //
      QDataStream clArchive(&clFile);
      //
      // Read the number
      //
      clArchive >> *ppclNVstore[0];
      //
      if( NvmGet(0, &iValue) )
      {
         if(iValue == iNumWnvms)
         {
            //
            // Archive has the correct number of elements: store all elements
            //
            while( iNvmNr<iNumWnvms )
            {
               clArchive >> *ppclNVstore[iNvmNr++];
            }
            fCC = true;
         }
      }
      clFile.close();
   }
   return(fCC);
}

//
//  Function:  NvmWrite
//  Purpose:   write the NVM to the data file
//
//  Parms:     File pathname
//  Returns:   true if OKee 
//
bool CNvmStorage::NvmWrite(QString &clFilename)
{
   QByteArray baTemp = clFilename.toUtf8();
   char *pcFilename  = baTemp.data();

   return( NvmWrite(pcFilename) );
}

//
//  Function:  NvmWrite
//  Purpose:   write the NVM to the data file
//
//  Parms:     File pathname
//  Returns:   true if OKee 
//
bool CNvmStorage::NvmWrite(char *pcFilename)
{
   bool     fCc=false;
   int      iNvmNr=0;

   QFile clFile(pcFilename);
   if( clFile.open(QIODevice::WriteOnly) )
   {
      //
      // Write the data serialized from the file
      //
      QDataStream clArchive(&clFile);
      //
      // Store actual number of element
      //
      NvmPut(0, iNumWnvms);
      while( iNvmNr<iNumWnvms )
      {
         clArchive << *ppclNVstore[iNvmNr];
         iNvmNr++;
      }
      clFile.close();
      fCc = true;
   }
   return(fCc);
}

//
//  Function:  NvmPut
//  Purpose:   Put a value into a NVM storage location
//
//  Parms:     NVM-ID, QString with the data to store
//  Returns:
//
void CNvmStorage::NvmPut(int iNvmNr, QString clStr)
{
   QString *pclTmp;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) )
   {
      pclTmp   = ppclNVstore[iNvmNr];
      *pclTmp  = clStr;
      NvmRunCallbacks(iNvmNr);
      emit NvmChanged(iNvmNr);
   }
}

//
//  Function:  NvmPut
//  Purpose:   Put a bool value into a NVM storage location
//
//  Parms:     NVM-ID, int to store
//  Returns:
//
void CNvmStorage::NvmPut(int iNvmNr, bool fValue)
{
   QString *pclTmp;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) )
   {
      pclTmp = ppclNVstore[iNvmNr];
      if(fValue) *pclTmp = "1";
      else       *pclTmp = "0";
      NvmRunCallbacks(iNvmNr);
      emit NvmChanged(iNvmNr);
   }
}

//
//  Function:  NvmPut
//  Purpose:   Put a int value into a NVM storage location
//
//  Parms:     NVM-ID, int to store
//  Returns:
//
void CNvmStorage::NvmPut(int iNvmNr, int iValue)
{
   QString *pclTmp;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) )
   {
      pclTmp = ppclNVstore[iNvmNr];
      pclTmp->sprintf("%d", iValue);
      NvmRunCallbacks(iNvmNr);
      emit NvmChanged(iNvmNr);
   }
}

//
//  Function:  NvmPut
//  Purpose:   Put a fp value into a NVM storage location
//
//  Parms:     NVM-ID, int to store
//  Returns:
//
void CNvmStorage::NvmPut(int iNvmNr, double flValue)
{
   QString *pclTmp;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) )
   {
      pclTmp = ppclNVstore[iNvmNr];
      pclTmp->sprintf("%f", flValue);
      NvmRunCallbacks(iNvmNr);
      emit NvmChanged(iNvmNr);
   }
}

//
//  Function:  NvmPut
//  Purpose:   Put a value into a NVM storage location
//
//  Parms:     NVM-ID, Byte to store
//  Returns:
//
void CNvmStorage::NvmPut(int iNvmNr, quint8 ubValue)
{
   QString *pclTmp;


   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) )
   {
      pclTmp = ppclNVstore[iNvmNr];
      pclTmp->sprintf("%02x", ubValue);
      NvmRunCallbacks(iNvmNr);
      emit NvmChanged(iNvmNr);
   }
}

//
//  Function:  NvmPut
//  Purpose:   Put a value into a NVM storage location
//
//  Parms:     NVM-ID, Ptr to asciiZ with the textstring to store
//  Returns:
//
void CNvmStorage::NvmPut(int iNvmNr, char *pcData)
{
   QString *pclTmp;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) )
   {
      pclTmp = ppclNVstore[iNvmNr];
      pclTmp->sprintf("%s", pcData);
      NvmRunCallbacks(iNvmNr);
      emit NvmChanged(iNvmNr);
   }
}

//
//  Function:  NvmPut
//  Purpose:   Put a value into a NVM storage location
//
//  Parms:     NVM-ID, Ptr to Byte array plus number to store
//  Returns:
//
void CNvmStorage::NvmPut(int iNvmNr, quint8 *pubValue, int iSize)
{
   int      iNr=0;
   QString  clStr, clAll;
   QString *pclTmp;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) )
   {
      while(iSize--)
      {
         clStr.sprintf("%02x,", pubValue[iNr++]);
         clAll += clStr;
      }
      //
      // s = "xx,xx,xx,xx,xx,xx,xx,xx,xx, .... ,xx,"
      // Remove last ","
      //
      clAll.chop(1);
      pclTmp  = ppclNVstore[iNvmNr];
      *pclTmp = clAll;
      NvmRunCallbacks(iNvmNr);
      emit NvmChanged(iNvmNr);
   }
}

//
//  Function:  NvmPutFlag
//  Purpose:   Put a flag into a NVM storage location
//
//  Parms:     NVM-ID, value
//  Returns:
//
void CNvmStorage::NvmPutFlag(int iNvmNr, bool fValue)
{
   QString *pclTmp;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) )
   {
      pclTmp = ppclNVstore[iNvmNr];
      if(fValue) *pclTmp = "1";
      else       *pclTmp = "0";
      //
      NvmRunCallbacks(iNvmNr);
      emit NvmChanged(iNvmNr);
   }
}

//
//  Function:  NvmGet
//  Purpose:   Get a bool value from a NVM storage location
//
//  Parms:     NVM-ID, Ptr to int to receive the data
//  Returns:   true if data correctly read
//
bool CNvmStorage::NvmGet(int iNvmNr, bool *pfValue)
{
   bool        fOkee;
   int         iVal;
   QString    *pclTmp;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) && pfValue)
   {
      pclTmp = ppclNVstore[iNvmNr];
      iVal = pclTmp->toInt(&fOkee, 10);
      if(fOkee)
      {
         if(iVal) *pfValue = true;
         else     *pfValue = false;
         return(true);
      }
   }
   return(false);
}

//
//  Function:  NvmGet
//  Purpose:   Get a int value from a NVM storage location
//
//  Parms:     NVM-ID, Ptr to int to receive the data
//  Returns:   true if data correctly read
//
bool CNvmStorage::NvmGet(int iNvmNr, int *piValue)
{
   bool        fOkee;
   int         iVal;
   QString    *pclTmp;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) && piValue)
   {
      pclTmp = ppclNVstore[iNvmNr];
      iVal = pclTmp->toInt(&fOkee, 10);
      if(fOkee)
      {
         *piValue = iVal;
         return(true);
      }
   }
   return(false);
}

//
//  Function:  NvmGet
//  Purpose:   Get a fp value from a NVM storage location
//
//  Parms:     NVM-ID, Ptr to fp to receive the data
//  Returns:   true if data correctly read
//
bool CNvmStorage::NvmGet(int iNvmNr, double *pflValue)
{
   //pwjh int         iNr;
   bool        fOkee;
   double      flVal;
   QString    *pclTmp;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) && pflValue)
   {
      pclTmp    = ppclNVstore[iNvmNr];

      //pwjh iNr       = sscanf(pclTmp->toLatin1(), "%f", &flVal);
      //pwjh if(iNr == 1)

      flVal = pclTmp->toDouble(&fOkee);
      if(fOkee)
      {
         *pflValue = flVal;
         return(true);
      }
   }
   return(false);
}

//
//  Function:  NvmGet
//  Purpose:   Get a value from a NVM storage location
//
//  Parms:     NVM-ID, Ptr to Byte to receive the data
//  Returns:   true if data correctly read
//
bool CNvmStorage::NvmGet(int iNvmNr, quint8 *pubDest)
{
   //pwjh int      iNr;
   bool     fOkee;
   int      iVal;
   QString *pclTmp;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) && pubDest)
   {
      pclTmp = ppclNVstore[iNvmNr];
      
      //pwjh iNr       = sscanf(pclTmp->toLatin1(), "%x", &iVal);
      //pwjh if(iNr == 1)

      iVal = pclTmp->toInt(&fOkee, 16);
      if(fOkee)
      {
         *pubDest = (quint8) iVal;
         return(true);
      }
   }
   return(false);
}

//
//  Function:  NvmGet
//  Purpose:   Get text from a NVM storage location
//
//  Parms:     NVM-ID, Ptr to textbuffer to receive the data
//             The buffer MUST be pre-formatted with 0x20 to take the data.
//  Returns:   true if data correctly read
//
bool CNvmStorage::NvmGet(int iNvmNr, char *pcDest)
{
   bool        fCC=false;
   char       *pcBuffer;
   size_t      iSize=0;
   char       *pcStorage;
   QString    *pclTmp;
   QByteArray clArr;

   //
   // Get NVM data in the user buffer.
   //
   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) && pcDest)
   {
      pclTmp    = ppclNVstore[iNvmNr];
      clArr     = pclTmp->toLatin1();
      pcStorage = clArr.data();
      //
      // Count the destination size
      //
      pcBuffer = pcDest;
      while(*pcBuffer++ == 0x20) iSize++;
      //
      //copy the storage into the buffer
      //
      strncpy(pcDest, pcStorage, iSize);
      fCC = true;
   }
   return(fCC);
}

//
//  Function:  NvmGet
//  Purpose:   Get byte array from a NVM storage location
//
//  Parms:     NVM-ID, Ptr to buffer to receive the data, number of members
//  Returns:   true if data correctly read
//
bool CNvmStorage::NvmGet(int iNvmNr, quint8 *pcDest, int iSize)
{
   bool        fCC=true;
   bool        fOkee;
   //int         iNr;
   int         iVal;
   char       *pcStorage;
   QString    *pclTmp;
   QByteArray  clArr;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) && pcDest)
   {
      pclTmp    = ppclNVstore[iNvmNr];
      clArr     = pclTmp->toLatin1();
      pcStorage = clArr.data();
      //
      // pcStorage -> "xx,xx,xx,xx,xx,xx,xx,xx,xx, .... ,xx,"
      //
      while(fCC && iSize--)
      {
         //pwjh iNr = sscanf(pcStorage, "%x", &iVal);
         //pwjh if(iNr == 1)

         iVal = pclTmp->toInt(&fOkee, 10);
         if(fOkee)
         {
            *pcDest++  = static_cast<char>(iVal & 0xff);
            pcStorage += 3;
         }
         else
         {
            fCC = false;
         }
      }
   }
   return(fCC);
}

//
//  Function:  NvmGet
//  Purpose:   Get QString from a NVM storage location
//
//  Parms:     NVM-ID, Ptr to QString receive the data
//  Returns:   true if data correctly read
//
bool CNvmStorage::NvmGet(int iNvmNr, QString *pclStr)
{
   QString *pclTmp;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) && pclStr)
   {
      pclTmp  = ppclNVstore[iNvmNr];
      *pclStr = *pclTmp;
      return(true);
   }
   return(false);
}

//
//  Function:  NvmGetFlag
//  Purpose:   Test flag from a NVM storage location
//
//  Parms:     NVM-ID
//  Returns:   true if data correctly read
//
bool CNvmStorage::NvmGetFlag(int iNvmNr)
{
   bool        fOkee=false;
   int         iVal;
   QString    *pclTmp;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) )
   {
      pclTmp = ppclNVstore[iNvmNr];
      iVal   = pclTmp->toInt(&fOkee, 10);
      if(fOkee) fOkee = (iVal != 0);
   }
   return(fOkee);
}

//
//  Function:  NvmElementLength
//  Purpose:   Get the size of an element in the NVM storage
//
//  Parms:     NVM-ID
//  Returns:   Size
//
int CNvmStorage::NvmElementLength(int iNvmNr)
{
   int iSize = 0;

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) )
   {
        iSize = ppclNVstore[iNvmNr]->length();
   }
   return(iSize);
}

//
//  Function:  NvmNrOfElements
//  Purpose:   Get the number of elements in the NVM storage
//
//  Parms:
//  Returns:   Number of elements
//
int CNvmStorage::NvmNrOfElements()
{
   return(iNumWnvms);
}

//
//  Function:  NvmRunCallbacks
//  Purpose:   Run all callback functions
//
//  Parms:     Element index 0..iNumWnvms-1
//  Returns:   
//
void CNvmStorage::NvmRunCallbacks(int iNvmNr)
{
   int         iNr=0;
   TFunctor   *pfunCbs;

   while(iNr < iNumCallbacks)
   {
      pfunCbs = ppfunCallbacks[iNr++];
      if(pfunCbs) pfunCbs->Call(iNvmNr);
      else        break;
   }
}

//
//  Function:  NvmRunTests
//  Purpose:   Run all API calls
//
//  Parms:     Element index 0..iNumWnvms-1
//  Returns:   Number of errors
//
int CNvmStorage::NvmRunTests(int iNvmNr)
{
   int      iNumErrors=0;
   int      iSrc, iDest, iLen;
   quint8   ubSrc, ubDest;
   QString  clSrc, clDest;
   char     pcBuffer[32];
   quint8   pubBuffer[8];

   const char    *pcSpc     = "                               ";
   const char    *pcSrc     = "0123456789012345678901234567890";
   const quint8   pubSrc[8] = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88};

   if( (iNumWnvms > 0) && (iNvmNr < iNumWnvms) )
   {
      //
      // void     NvmPut            (int, QString);
      // bool     NvmGet            (int, QString *);
      //
      clSrc.sprintf("QSting test element %d", iNvmNr);
      NvmPut(iNvmNr, clSrc);
      NvmGet(iNvmNr, &clDest);
      if(clSrc.compare(clDest) != 0) iNumErrors++;
      //
      // void     NvmPut            (int, int);
      // bool     NvmGet            (int, int *);
      //
      iSrc = 0xdeadbeef;
      NvmPut(iNvmNr, iSrc);
      NvmGet(iNvmNr, &iDest);
      if(iDest != iSrc) iNumErrors++;
      //
      // void     NvmPut            (int, quint8);
      // bool     NvmGet            (int, quint8 *);
      //
      ubSrc = 0xaa;
      NvmPut(iNvmNr, ubSrc);
      NvmGet(iNvmNr, &ubDest);
      if(ubDest != ubSrc) iNumErrors++;
      //
      // void     NvmPut            (int, char *);
      // bool     NvmGet            (int, char *);
      //
      NvmPut(iNvmNr, pcSrc);
      iLen = NvmElementLength(iNvmNr);
      strcpy(pcBuffer, pcSpc);
      NvmGet(iNvmNr, pcBuffer);
      if(NvmElementLength(iNvmNr) != iLen) iNumErrors++;
      //
      // void     NvmPut            (int, quint8 *, int);
      // bool     NvmGet            (int, quint8 *, int);
      //
      NvmPut(iNvmNr, (quint8 *)pubSrc, 8);
      NvmGet(iNvmNr, pubBuffer, 8);
      if(memcmp(pubSrc, pubBuffer, 8) != 0) iNumErrors++;
   }
   return(iNumErrors);
}
