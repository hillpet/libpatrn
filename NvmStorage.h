/*  (c) Copyright:  2014...2021 Patrn ESS
**
**  $Workfile:  NvmStorage.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Header file for the *.cpp
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  24 Feb 2014         Created
 *  24 Feb 2021         Add Put/Get double
 *  17 Mar 2021         Add NvmGetFlag/NvmPutFlag
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_NVMSTORAGE_H_)
#define _NVMSTORAGE_H_

#include <QDialog>
#include <QWidget>
#include <QtCore>
#include <QDataStream>

#include "libpatrn.h"

typedef QString *PQSTR;

class TFunctor
{
public:

   // two possible functions to call member function. virtual cause derived
   // classes will use a pointer to an object and a pointer to a member function
   // to make the function call
   virtual void operator()(int iNum)=0;  // call using operator
   virtual void Call(int iNum)=0;        // call using function
};


// derived template class
template <class TClass> class TSpecificFunctor : public TFunctor
{
private:
   void (TClass::*fpt)(int);           // pointer to member function
   TClass* pt2Object;                  // pointer to object

public:

   // constructor - takes pointer to an object and pointer to a member and stores
   // them in two private variables
   TSpecificFunctor(TClass* _pt2Object, void(TClass::*_fpt)(int))
      { pt2Object = _pt2Object;  fpt=_fpt; };

   // override operator "()"
   virtual void operator()(int iNum)
    { (*pt2Object.*fpt)(iNum);};                // execute member function

   // override function "Call"
   virtual void Call(int iNum)
     { (*pt2Object.*fpt)(iNum);};               // execute member function
};

//
// The Class for handling permanent private storage of application data in the document
//
class CNvmStorage : public QWidget
{
    Q_OBJECT

public:
   CNvmStorage();
   CNvmStorage(int);
   CNvmStorage(QWidget *pclParent);
   CNvmStorage(QWidget *pclParent, int);
   virtual ~CNvmStorage();

public:
   virtual  void NvmHasChanged(int);
   //
   void     NvmSetCallbacks   (TFunctor **, int);
   void     NvmSetCallbacks   (TFunctor **);
   //
   void     NvmPut            (int, bool);
   void     NvmPut            (int, int);
   void     NvmPut            (int, double);
   void     NvmPut            (int, QString);
   void     NvmPut            (int, quint8);
   void     NvmPut            (int, char *);
   void     NvmPut            (int, quint8 *, int);
   void     NvmPutFlag        (int, bool);
   //
   int      NvmNrOfElements   ();
   int      NvmElementLength  (int);
   //
   bool     NvmGet            (int, bool *);
   bool     NvmGet            (int, int *);
   bool     NvmGet            (int, double *);
   bool     NvmGet            (int, QString *);
   bool     NvmGet            (int, quint8 *);
   bool     NvmGet            (int, char *);
   bool     NvmGet            (int, quint8 *, int);
   bool     NvmGetFlag        (int);
   bool     NvmWrite          (char *);
   bool     NvmWrite          (QString &);
   bool     NvmRead           (char *);
   bool     NvmRead           (QString &);
   //
   int      NvmRunTests       (int);

private:
   void     NvmRunCallbacks   (int);

private slots:

signals:
   void     NvmChanged        (int);

private:
   static   int         iInstance;
   static   PQSTR      *ppclNVstore;
   static   int         iNumWnvms;
   static   int         iNumCallbacks;
   static   TFunctor  **ppfunCallbacks;

};

#endif // !defined(_NVMSTORAGE_H_)
