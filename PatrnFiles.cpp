/*  (c) Copyright:  2014..2020  Patrn ESS, Confidential Data
 *
 *  Workfile:           PatrnFiles.cpp
 *  Purpose:            Misc support functions
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:       QT5.10
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    15 Aug 2014: Created
 *    17 Apr 2020: Add RTC HH:MM
 *    26 Aug 2020: Add RTC_SecsToDate
 *    03 Mar 2023: Add LOG timestamp; Add GetInstances
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <QDateTime>
#include <QTimeZone>
#include <QDate>
#include <QTime>
#include "PatrnFiles.h"

//
// Static declarations
//
bool        CPatrn::fLogFileOpen;
int         CPatrn::iLogInstance;
QString     *CPatrn::pclLogName;
QTextStream *CPatrn::pclLogStream;
QFile       *CPatrn::pclLogFile;
//
static const char *pcTypeIsInteger     = "integer";
static const char *pcTypeIsNumber      = "number";
static const char *pcTypeIsBool        = "bool";
static const char *pcTypeIsEnum        = "enum";
static const char *pcTypeIsObject      = "object";
static const char *pcTypeIsString      = "string";
//
// JSON fields
//
static const char *pcKeyIsEnum         = "enum";
static const char *pcKeyIsOption       = "jsonoption";
static const char *pcKeyIsSelected     = "selected";
static const char *pcKeyIsType         = "type";
static const char *pcKeyIsValue        = "value";
static const char *pcKeyIsNumParms     = "numparms";

//
//  Function:  PatrnFiles constructor
//  Purpose:   
//
//  Parms:     Log full pathname or NULL for default 
//  Returns:
//
CPatrn::CPatrn(QString *pclFile)
{
   pclLogName = new QString;
   if(pclFile) *pclLogName = *pclFile;
   else        *pclLogName = "E:/Users/Peter/Proj/Qt5/Logfiles/Default.log";
   //
   LIB_Init();
}

//
//  Function:  PatrnFiles constructor
//  Purpose:   
//
//  Parms:     
//  Returns:
//
CPatrn::CPatrn()
{
   if(!pclLogName)
   {
      pclLogName  = new QString;
      *pclLogName = "E:/Users/Peter/Proj/Qt5/Logfiles/Default.log";
   }
   LIB_Init();
}

//
//  Function:  PatrnFiles destructor
//  Purpose:   
//
//  Parms:     
//  Returns:
//
CPatrn::~CPatrn()
{
   if(--iLogInstance == 0)
   {
      //
      // The final instance cleans up
      //
      LOG_FileClose();
      //
      if(pclLogName)    { delete pclLogName;   pclLogName   = nullptr; }
      if(pclLogStream)  { delete pclLogStream; pclLogStream = nullptr; }
      if(pclLogFile)    { delete pclLogFile;   pclLogFile   = nullptr; }
   }
}

//
//  Function:  MSG_AddText
//  Purpose:   Clear the xxx_Message widget
//  Parms:     Widget item
//  Returns:   Nr of LIB instances
//
int CPatrn::LIB_GetInstances()
{
   return(iLogInstance);
}

//
//  Function:  PatrnFiles init
//  Purpose:   
//
//  Parms:     
//  Returns:
//
void CPatrn::LIB_Init()
{
   iLogInstance++;
   //
   fLogFileOpen = false;
   pclLogStream = nullptr;
   pclLogFile   = nullptr;
   //
   fLogTimestamp  = true;
   fMsgTimestamp  = true;
   cDateDelimiter = '-';
}


/* ======   Local   Functions separator ===========================================
_______WIDGET_FUNCTIONS(){}
======================X===========================================================*/

//
//  Function:  MSG_AddText
//  Purpose:   Clear the xxx_Message widget
//  Parms:     Widget item
//  Returns:   
//
void CPatrn::MSG_AddText(QPlainTextEdit *pclDlg)
{
   pclDlg->clear();
}

//
//  Function:  MSG_AddText
//  Purpose:   Add textstring to the xxx_Message widget
//  Parms:     Widget item, text
//  Returns:   
//
void CPatrn::MSG_AddText(QPlainTextEdit *pclDlg, QString clStr)
{
   QString  clTmp;

   if(fMsgTimestamp) 
   {
      clTmp  = RTC_GetTimeDateStamp();
      clTmp += ": ";
   }
   clTmp += clStr;
   pclDlg->appendPlainText(clTmp);
}

//
//  Function:  MSG_AddText
//  Purpose:   Add textstring to the xxx_Message widget
//  Parms:     Widget item, text, vars
//  Returns:   
//
void CPatrn::MSG_AddText(QPlainTextEdit *pclDlg, QString clStr, QString clVar)
{
   QString  clTmp;

   if(fMsgTimestamp) 
   {
      clTmp  = RTC_GetTimeDateStamp();
      clTmp += ": ";
   }
   clTmp += clStr;
   clTmp += clVar;
   pclDlg->appendPlainText(clTmp);
}

//
//  Function:  MSG_AddText
//  Purpose:   Add textstring to the xxx_Message widget
//  Parms:     Widget item, text, vars
//  Returns:   
//
void CPatrn::MSG_AddText(QPlainTextEdit *pclDlg, QString clStr, qint64 llVar)
{
   QString  clTmp, clVar;

   if(fMsgTimestamp) 
   {
      clTmp  = RTC_GetTimeDateStamp();
      clTmp += ": ";
   }
   clTmp += clStr;
   clVar.sprintf("%lld", llVar);
   clTmp += clVar;
   pclDlg->appendPlainText(clTmp);
}

//
//  Function:  MSG_AddText
//  Purpose:   Add textstring to the xxx_Message widget
//  Parms:     Widget item, text, vars
//  Returns:   
//
void CPatrn::MSG_AddText(QPlainTextEdit *pclDlg, QString clStr, int iVar)
{
   QString  clTmp, clVar;

   if(fMsgTimestamp) 
   {
      clTmp  = RTC_GetTimeDateStamp();
      clTmp += ": ";
   }
   clTmp += clStr;
   clVar.sprintf("%d", iVar);
   clTmp += clVar;
   pclDlg->appendPlainText(clTmp);
}

//
//  Function:  MSG_AddText
//  Purpose:   Add textstring to the xxx_Message widget
//  Parms:     Widget item, text, vars
//  Returns:   
//
void CPatrn::MSG_AddText(QPlainTextEdit *pclDlg, QString clStr, double flVar)
{
   QString  clTmp, clVar;

   if(fMsgTimestamp) 
   {
      clTmp  = RTC_GetTimeDateStamp();
      clTmp += ": ";
   }
   clTmp += clStr;
   clVar.sprintf("%.3f", flVar);
   clTmp += clVar;
   pclDlg->appendPlainText(clTmp);
}

//
//  Function:  MSG_Timestamp
//  Purpose:   En/disable timestamps
//  Parms:     enable=true
//  Returns:   
//
void CPatrn::MSG_Timestamp(bool fEnable)
{
   fMsgTimestamp = fEnable;
}

/* ======   Local   Functions separator ===========================================
__________LOG_FUNCTIONS(){}
======================X===========================================================*/

//
//  Function:  LOG_FileOpen
//  Purpose:   Open the Log file
//
//  Parms:     
//  Returns:   true if open
//
bool CPatrn::LOG_FileOpen()
{
   return(LOG_FileOpen('a'));
}

//
//  Function:  LOG_FileOpen
//  Purpose:   Open the Log file
//
//  Parms:     (a)ppend or (t)runcate
//  Returns:   true if open
//
bool CPatrn::LOG_FileOpen(char cMode)
{
   if(!fLogFileOpen)
   {
      //
      // LOG file not yet open: do so
      //
      if(pclLogName->length())
      {
         // We have a filename: use it
         pclLogFile = new QFile;
         pclLogFile->setFileName(*pclLogName);
         switch(cMode)
         {
            default:
            case 'a':
               fLogFileOpen = pclLogFile->open(QIODevice::Append);
               break;

            case 't':
               fLogFileOpen = pclLogFile->open(QIODevice::Truncate);
               break;
         }
         if(fLogFileOpen)
         {
            pclLogStream = new QTextStream;
            pclLogStream->setDevice(pclLogFile);
         }
      }
   }
   return(fLogFileOpen);
}

//
//  Function:  LOG_FileClose
//  Purpose:   Close the Log output file
//
//  Parms:     
//  Returns:
//
void CPatrn::LOG_FileClose()
{
   if(fLogFileOpen)
   {
      LOG_FileWrite("-- Log end -------------------------------------------------\n\n");
      pclLogFile->flush();
      pclLogFile->close();
      fLogFileOpen = false;
   }
}

//
//  Function:  LOG_FileFlush
//  Purpose:   Flush the Log output file
//
//  Parms:     
//  Returns:
//
void CPatrn::LOG_FileFlush()
{
   if(fLogFileOpen)
   {
      pclLogFile->flush();
   }
}

//
//  Function:  LOG_FileWrite
//  Purpose:   Write a string to the Log file
//
//  Parms:     String
//  Returns:   Nr written (-1 on error)
//
int CPatrn::LOG_FileWrite(char *pcLog)
{
   QString  clStr;

   clStr = pcLog;
   return(LOG_FileWrite(&clStr, 1));
}

//
//  Function:  LOG_FileWrite
//  Purpose:   Write a string to the Log file
//
//  Parms:     String, LF's
//  Returns:   Nr written (-1 on error)
//
int CPatrn::LOG_FileWrite(char *pcLog, int iNr)
{
   QString  clStr;

   clStr = pcLog;
   return(LOG_FileWrite(&clStr, iNr));
}

//
//  Function:  LOG_FileWrite
//  Purpose:   Write a string to the Log file
//
//  Parms:     String
//  Returns:   Nr written (-1 on error)
//
int CPatrn::LOG_FileWrite(const char *pcLog)
{
   QString  clStr;

   clStr = pcLog;
   return(LOG_FileWrite(&clStr, 1));
}

//
//  Function:  LOG_FileWrite
//  Purpose:   Write a string to the Log file
//
//  Parms:     String, nr LF's
//  Returns:   Nr written (-1 on error)
//
int CPatrn::LOG_FileWrite(const char *pcLog, int iNr)
{
   QString  clStr;

   clStr = pcLog;
   return(LOG_FileWrite(&clStr, iNr));
}

//
//  Function:  LOG_FileWrite
//  Purpose:   Write strings to the Log file
//
//  Parms:     String, string
//  Returns:   Nr written (-1 on error)
//
int CPatrn::LOG_FileWrite(const char *pcLog, QString clTxt)
{
   QString  clStr;

   clStr  = pcLog;
   clStr += clTxt;
   return(LOG_FileWrite(&clStr));
}

//
//  Function:  LOG_FileWrite
//  Purpose:   Write a string to the Log file
//
//  Parms:     QString ptr
//  Returns:   Nr written (-1 on error)
//
int CPatrn::LOG_FileWrite(QString *pclLog)
{
   return(LOG_FileWrite(pclLog, 1));
}

//
//  Function:  LOG_FileWrite
//  Purpose:   Write a string to the Log file plus CRLF's
//
//  Parms:     QString ptr, Nr of LFs
//  Returns:   Nr written (-1 on error)
//
int CPatrn::LOG_FileWrite(QString *pclLog, int iNr)
{
   bool  fTimestamp=false;
   int   iLen=0;

   if(!fLogFileOpen)
   {
      // LOG file not yet open
      LOG_FileOpen();
      fTimestamp = true;
   }
   if(fLogFileOpen)
   {
      if(fTimestamp)
      {
         // "--Log---- 03-03-2023 14:51:34 ------------------------------"
         *pclLogStream << "--Log---- " << RTC_GetTimeDateStamp() << " ------------------------------\n";
      }
      if(fLogTimestamp) *pclLogStream << "[" << RTC_GetTimeDateStamp() << "] " << *pclLog;
      else              *pclLogStream << *pclLog;
      //
      iLen = pclLog->size();
      while(iNr--)
      {
         *pclLogStream << "\n";
         iLen++;
      }
   }
   return(iLen);
}

//
//  Function:  LOG_Timestamp
//  Purpose:   En/disable timestamps
//  Parms:     enable=true
//  Returns:   
//
void CPatrn::LOG_Timestamp(bool fEnable)
{
   fLogTimestamp = fEnable;
}


/* ======   Local   Functions separator ===========================================
__________LIB_FUNCTIONS(){}
======================X===========================================================*/

//
//  Function:  LIB_BuildOptionsList
//  Purpose:   Build the options list containing the current selected parameter value options
//
//  Parms:     Json ptr
//  Returns:   Options list with format:
//             "&Options=1234,234,1&Option2=File"
//
QString CPatrn::LIB_BuildOptionsList(CPiJson *pclJson)
{
   int         iNumParms, iIdx, iNumObjs, iVal;
   PTYPE       tType;
   QString     clOpt, clStr;

   iNumObjs = pclJson->GetNumRootObjects();
   //
   for (int i=0; i<iNumObjs; i++)
   {
      if( pclJson->GetObjectInteger(i, pcKeyIsSelected) )
      {
         iNumParms = pclJson->GetObjectInteger(i, pcKeyIsNumParms);
         clStr = pclJson->GetObjectString(i, pcKeyIsOption);
         if( clStr.length() )
         {
            //
            // This option is a valid RPi option
            //
            tType = LIB_GetRootType(pclJson, i);
            switch(tType)
            {
               case PTYPE_BOOL:
                  // Insert only the option if true
                  iVal = pclJson->GetObjectInteger(i, pcKeyIsValue, 0);
                  if(iVal) 
                  {
                     clOpt += "&"; 
                     clOpt += clStr; 
                  }
                  break;

               default:
               case PTYPE_OBJECT:
               case PTYPE_STRING:
                  // No Valid types: ignore
                  break;

               case PTYPE_INTEGER:
                  // insert the option integer value as string
                  // Retrieve the object.member(s)
                  for(iIdx=0; iIdx<iNumParms; iIdx++)
                  {
                     iVal = pclJson->GetObjectInteger(i, pcKeyIsValue, iIdx);
                     if(iIdx)
                     {
                        // More than one value for this option: separate
                        clOpt += ",";
                     }
                     else
                     {
                        // First option: insert JSON Option string
                        clOpt += "&"; 
                        clOpt += clStr; 
                        clOpt += "=";
                     }
                     clStr.sprintf("%d", iVal);
                     clOpt += clStr; 
                  }
                  break;

               case PTYPE_ENUM:
                  // Insert currect option-enum
                  // Retrieve the object.member(s)
                  for(iIdx=0; iIdx<iNumParms; iIdx++)
                  {
                     iVal = pclJson->GetObjectInteger(i, pcKeyIsValue, iIdx);
                     if(iIdx)
                     {
                        // More than one value for this option: separate
                        clOpt += ",";
                     }
                     else
                     {
                        // First option: insert JSON Option string
                        clOpt += "&"; 
                        clOpt += clStr; 
                        clOpt += "=";
                     }
                     clOpt += pclJson->GetObjectString(i, pcKeyIsEnum, iVal); 
                  }
                  break;
            }
         }
      }
   }
   return(clOpt);
}

//
//  Function:  LIB_GetRootType
//  Purpose:   Get the PTYPE for this JSON object (root."type")
//
//  Parms:     Json ptr, Index of root key
//  Returns:   PTYPE
//
PTYPE CPatrn::LIB_GetRootType(CPiJson *pclJson, int iKey1)
{
   QString clKey1 = pclJson->GetObjectName(iKey1);
   return( LIB_GetRootType(pclJson, clKey1) );
}

//
//  Function:  LIB_GetRootType
//  Purpose:   Get the PTYPE for this JSON object (root."type")
//
//  Parms:     Json ptr, Root key
//  Returns:   PTYPE
//
PTYPE CPatrn::LIB_GetRootType(CPiJson *pclJson, QString clKey1)
{
   const char *pcKey1;
   QByteArray  baKey1;

   baKey1 = clKey1.toLocal8Bit();
   pcKey1 = baKey1.constData();
   return( LIB_GetRootType(pclJson, pcKey1) );
}

//
//  Function:  LIB_GetRootType
//  Purpose:   Get the PTYPE for this JSON object (root."type")
//
//  Parms:     Json ptr, Root key
//  Returns:   PTYPE
//
PTYPE CPatrn::LIB_GetRootType(CPiJson *pclJson, const char *pcKey1)
{
   JTYPE tJsonType;

   tJsonType = pclJson->GetObjectType(pcKey1, pcKeyIsType, static_cast<const char *>(Q_NULLPTR));
   if(tJsonType == JTYPE_STRING)
   {
      QString clStr = pclJson->GetValueString();
      //
      if(clStr.compare(pcTypeIsInteger, Qt::CaseInsensitive) == 0) return(PTYPE_INTEGER);
      if(clStr.compare(pcTypeIsString,  Qt::CaseInsensitive) == 0) return(PTYPE_STRING);
      if(clStr.compare(pcTypeIsEnum,    Qt::CaseInsensitive) == 0) return(PTYPE_ENUM);
      if(clStr.compare(pcTypeIsObject,  Qt::CaseInsensitive) == 0) return(PTYPE_OBJECT);
      if(clStr.compare(pcTypeIsBool,    Qt::CaseInsensitive) == 0) return(PTYPE_BOOL);
      if(clStr.compare(pcTypeIsNumber,  Qt::CaseInsensitive) == 0) return(PTYPE_NUMBER);
   }
   return(PTYPE_UNDEFINED);
}


/* ======   Local   Functions separator ===========================================
__________RTC_FUNCTIONS(){}
======================X===========================================================*/

const qint64   llLdapTo1970 = 116444700000000000;
const qint64   llLdapFactor = 10000000;

//
//  Function:  RTC_ConvertToLdap
//  Purpose:   Convert LDAP/NT timestamp to secs since 1970
//
//  Parms:     Secs since 1 Jan 1970
//  Returns:   LDAP/NT 18 digits timestamp
//  Note:      LDAP/NT has the number of nSecs since 1 Jan 1601 00:00:00
//
QString CPatrn::RTC_ConvertToLdap(int iSecs)
{
   qint64   llVal;
   QString clStr;

   llVal  = llLdapFactor * static_cast<qint64>(iSecs);
   llVal += llLdapTo1970;
   clStr = QString::number(llVal);
   return(clStr);   
}

//
//  Function:  RTC_ConvertFromLdap
//  Purpose:   Convert secs since 1970 to LDAP/NT timestamp
//
//  Parms:     LDAP/NT 18 digit timestamp
//  Returns:   Secs since 1 Jan 1970
//  Note:      LDAP/NT has the number of nSecs since 1 Jan 1601 00:00:00
//
int CPatrn::RTC_ConvertFromLdap(QString clStr)
{
   bool     fOkee;
   int      iVal=0;
   qint64   llVal;

   llVal = clStr.toLongLong(&fOkee);
   //
   if(fOkee) 
   {
      llVal -= llLdapTo1970;
      llVal /= llLdapFactor;
      iVal   = static_cast<int>(llVal);
   }
   return(iVal);
}

//
//  Function:  RTC_ConvertHms
//  Purpose:   Convert time format to int
//
//  Parms:     Time ptr, result ptr
//  Returns:   true if OKee
//
//  Note:      Time format hh:mm:ss
//             result =
//               -1 Illegal time
//                0 Time = --:--:--
//             else Time in Secs
//
bool CPatrn::RTC_ConvertHms(QString *pclStr, int *piTime)
{
   bool  fCc=true;
   int   iTime=0;

   if(pclStr->contains("--:--:--"))
   {
      fCc = false;
   }
   else
   {
      QVariant clVar(*pclStr);
      if( clVar.canConvert(QVariant::Time) )
      {
         QTime clTime = clVar.toTime();
         if(clTime.isValid())
         {
            iTime = (clTime.hour() * 3600) + (clTime.minute() * 60) + clTime.second() + 1;
         }
         else
         {
            iTime = pclStr->toInt(&fCc) + 1;
            if(!fCc) iTime = -1;
         }
      }
      else
      {
         iTime = -1;
      }
   }
   *piTime = iTime;
   return(fCc);
}

//
//  Function:  RTC_ConvertHm
//  Purpose:   Convert time format to int
//
//  Parms:     Time ptr, result ptr
//  Returns:   true if OKee
//
//  Note:      Time format hh:mm
//             result =
//               -1 Illegal time
//                0 Time = --:--
//             else Time in Secs
//
bool CPatrn::RTC_ConvertHm(QString *pclStr, int *piTime)
{
   bool  fCc=true;
   int   iTime=0;

   if(pclStr->contains("--:--"))
   {
      fCc = false;
   }
   else
   {
      QVariant clVar(*pclStr);
      if( clVar.canConvert(QVariant::Time) )
      {
         QTime clTime = clVar.toTime();
         if(clTime.isValid())
         {
            iTime = (clTime.hour() * 3600) + (clTime.minute() * 60);
         }
         else
         {
            iTime = pclStr->toInt(&fCc) + 1;
            if(!fCc) iTime = -1;
         }
      }
      else
      {
         iTime = -1;
      }
   }
   *piTime = iTime;
   return(fCc);
}

//
//  Function:  RTC_GetTimeDateFilename
//  Purpose:   Rework system T&D to a usable unique filename
//  Parms:     void
//  Returns:   T&D: "20110720-115959"
//
QString CPatrn::RTC_GetTimeDateFilename()
{
   QString     clStr;
   QDateTime   clTime = QDateTime::currentDateTime();

   clStr = clTime.toLocalTime().toString("yyyyMMdd-hhmmss");
   return(clStr);
}

//
//  Function:  RTC_GetTimeDateStamp
//  Purpose:   Rework system T&D to ascii
//  Parms:     void
//  Returns:   T&D: "24/12/2007 09:12:25"
//
QString CPatrn::RTC_GetTimeDateStamp()
{
   QString     clStr;
   QString     clFormat;
   QDateTime   clTime = QDateTime::currentDateTime();

   clFormat.sprintf("dd%cMM%cyyyy hh:mm:ss", cDateDelimiter, cDateDelimiter);
   //pwjh clStr = clTime.toLocalTime().toString("dd/MM/yyyy hh:mm:ss");
   clStr = clTime.toLocalTime().toString(clFormat);
   return(clStr);
}

//
//  Function:  RTC_GetDateStamp
//  Purpose:   Rework system T&D to ascii
//  Parms:     void
//  Returns:   T&D: "24/12/2007"
//
QString CPatrn::RTC_GetDateStamp()
{
   QString     clStr;
   QString     clFormat;
   QDateTime   clTime = QDateTime::currentDateTime();

   clFormat.sprintf("dd%cMM%cyyyy", cDateDelimiter, cDateDelimiter);
   //pwjh clStr = clTime.toLocalTime().toString("dd/MM/yyyy");
   clStr = clTime.toLocalTime().toString(clFormat);
   return(clStr);
}

//
//  Function:  RTC_GetTimeStamp
//  Purpose:   Rework system T&D to ascii
//  Parms:     void
//  Returns:   T&D: "09:12:25"
//
QString CPatrn::RTC_GetTimeStamp()
{
   QString     clStr;
   QDateTime   clTime = QDateTime::currentDateTime();

   clStr = clTime.toLocalTime().toString("hh:mm:ss");
   return(clStr);
}

//
//  Function:  RTC_GetTimeStampShort
//  Purpose:   Rework system T&D to ascii
//  Parms:     void
//  Returns:   T&D: "09:12"
//
QString CPatrn::RTC_GetTimeStampShort()
{
   QString     clStr;
   QDateTime   clTime = QDateTime::currentDateTime();

   clStr = clTime.toLocalTime().toString("hh:mm");
   return(clStr);
}

//
//  Function:  RTC_DateTimeToSecs
//  Purpose:   Convert current DateTime to secs
//
//  Parms:     
//  Returns:   Secs since 1/1/1970
//
//  Note:
//    qint64 QDateTime::currentMSecsSinceEpoch () [static]
//    QDateTime QDateTime::fromMSecsSinceEpoch ( qint64 msecs ) [static]
//
int CPatrn::RTC_DateTimeToSecs()
{
   int         iSecs;
   qint64      llmSecs;
   QTimeZone   clTimeZone;
   QDateTime   clDateTime = QDateTime::currentDateTime();
   
   llmSecs  = QDateTime::currentMSecsSinceEpoch();
   
   iSecs   = static_cast<int>(llmSecs/1000);
   if( (llmSecs%1000) >= 500) iSecs++;
   //
   // Add DST
   //
   iSecs += clTimeZone.daylightTimeOffset(clDateTime);
   return(iSecs);
}

//
//  Function:  RTC_DateTimeToSecs
//  Purpose:   Convert DateTime to secs
//
//  Parms:     Date
//  Returns:   Secs from  1/1/1970 until DateTime
//
//  Note:
//    qint64 QDateTime::currentMSecsSinceEpoch () [static]
//    QDateTime QDateTime::fromMSecsSinceEpoch ( qint64 msecs ) [static]
//
int CPatrn::RTC_DateTimeToSecs(QDate clDate)
{
   int         iSecs;
   qint64      llmSecs;
   QDateTime   clDateTime(clDate);
   QTimeZone   clTimeZone;
   
   llmSecs = clDateTime.toMSecsSinceEpoch();
   iSecs   = static_cast<int>(llmSecs/1000);
   if( (llmSecs%1000) >= 500) iSecs++;
   //
   // Add DST
   //
   iSecs += clTimeZone.daylightTimeOffset(clDateTime);
   return(iSecs);
}

//
//  Function:  SecsToDateTime
//  Purpose:   Convert Secs to DateTime
//
//  Parms:     buffer, secs
//  Returns:
//
void CPatrn::RTC_SecsToDateTime(QString *pclStr, int iSecs)
{
   QString     clStr;
   QString     clFormat;
   QDateTime   clDateTime;
   QTimeZone   clTimeZone;
   QDate       clDate;
   QTime       clTime;
   qint64      llmSecs;
   QDateTime   clDtNow = QDateTime::currentDateTime();

   iSecs     += clTimeZone.daylightTimeOffset(clDtNow);
   llmSecs    = 1000 * static_cast<qint64>(iSecs);
   clDateTime = QDateTime::fromMSecsSinceEpoch(llmSecs);
   clDate     = clDateTime.date();
   clTime     = clDateTime.time();

   clFormat.sprintf("%%02d%c%%02d%c%%04d  %%02d:%%02d:%%02d", cDateDelimiter, cDateDelimiter);
   //pwjh pclStr->sprintf("%02d-%02d-%04d  %02d:%02d:%02d",
   pclStr->sprintf(clFormat.toUtf8().constData(),
                     clDate.day(),
                     clDate.month(),
                     clDate.year(),
                     clTime.hour(),
                     clTime.minute(),
                     clTime.second() );
}

//
//  Function:  RTC_SecsToDate
//  Purpose:   Convert Secs to Date format "YYYYMMDD"
//
//  Parms:     buffer, secs
//  Returns:
//
void CPatrn::RTC_SecsToDate(QString *pclStr, int iSecs)
{
   qint64      llmSecs;
   QString     clStr;
   QDateTime   clDateTime;
   QDate       clDate;
   QTimeZone   clTimeZone;
   QDateTime   clDtNow = QDateTime::currentDateTime();

   iSecs     += clTimeZone.daylightTimeOffset(clDtNow);
   llmSecs    = 1000 * static_cast<qint64>(iSecs);
   clDateTime = QDateTime::fromMSecsSinceEpoch(llmSecs);
   clDate     = clDateTime.date();

   pclStr->sprintf("%04d%02d%02d",
                        clDate.year(),
                        clDate.month(),
                        clDate.day() );
}

//
//  Function:  RTC_SecsToDate
//  Purpose:   Convert Secs to Date
//
//  Parms:     Secs, Year^, Month^, Day^
//  Returns:
//
void CPatrn::RTC_SecsToDate(int iSecs, int *piYear, int *piMonth, int *piDay)
{
   QDateTime   clDateTime;
   QDate       clDate;
   qint64      llmSecs;

   llmSecs    = 1000 * static_cast<qint64>(iSecs);
   clDateTime = QDateTime::fromMSecsSinceEpoch(llmSecs);
   clDate     = clDateTime.date();
   //
   if(piYear)   *piYear  = clDate.year();
   if(piMonth)  *piMonth = clDate.month();
   if(piDay)    *piDay   = clDate.day();
}

//
//  Function:  RTC_SecsToTime
//  Purpose:   Convert Secs to Time format "HHMMSS"
//
//  Parms:     buffer, secs
//  Returns:
//
void CPatrn::RTC_SecsToTime(QString *pclStr, int iSecs)
{
   qint64      llmSecs;
   QString     clStr;
   QDateTime   clDateTime;
   QTime       clTime;
   QTimeZone   clTimeZone;
   QDateTime   clDtNow = QDateTime::currentDateTime();

   iSecs     += clTimeZone.daylightTimeOffset(clDtNow);
   llmSecs    = 1000 * static_cast<qint64>(iSecs);
   clDateTime = QDateTime::fromMSecsSinceEpoch(llmSecs);
   clTime     = clDateTime.time();

   pclStr->sprintf("%02d%02d%02d",
                        clTime.hour(),
                        clTime.minute(),
                        clTime.second() );
}

//
//  Function:  RTC_SecsToTime
//  Purpose:   Convert Secs to Time
//
//  Parms:     Secs, Hrs^, Min^, Sec^
//  Returns:
//
void CPatrn::RTC_SecsToTime(int iSecs, int *piHrs, int *piMin, int *piSec)
{
   qint64      llmSecs;
   QString     clStr;
   QDateTime   clDateTime;
   QTime       clTime;
   QTimeZone   clTimeZone;
   QDateTime   clDtNow = QDateTime::currentDateTime();

   iSecs     += clTimeZone.daylightTimeOffset(clDtNow);
   llmSecs    = 1000 * static_cast<qint64>(iSecs);
   clDateTime = QDateTime::fromMSecsSinceEpoch(llmSecs);
   clTime     = clDateTime.time();

   if(piHrs)  *piHrs = clTime.hour();
   if(piMin)  *piMin = clTime.minute();
   if(piSec)  *piSec = clTime.second();
}

//
//  Function:  RTC_SecsUntilBeginYear
//  Purpose:   Return secs from 1970 until Last 1 Jan 00:00:00
//
//  Parms:     
//  Returns:
//
int CPatrn::RTC_SecsUntilBeginYear()
{
   int         iSecs, iYear;
   QDate       clDateNow;
   QDateTime   clDtBeginYear;
   QDateTime   clDtNow = QDateTime::currentDateTime();

   clDtNow   = QDateTime::currentDateTime();
   clDateNow = clDtNow.toLocalTime().date();
   iYear     = clDateNow.year();

   QDate clDateBeginYear(iYear, 1, 1);
   clDtBeginYear.setDate(clDateBeginYear);
   qint64 llSecs = clDtBeginYear.toSecsSinceEpoch();
   //
   iSecs  = static_cast<int>(llSecs);
   return(iSecs);

}

//
//  Function:  RTC_SecsSinceBeginYear
//  Purpose:   Return secs from Last 1 Jan 00:00:00 until now
//
//  Parms:     
//  Returns:
//
int CPatrn::RTC_SecsSinceBeginYear()
{
   int         iSecs, iYear;
   QDate       clDateNow;
   QDateTime   clDtBeginYear;
   QDateTime   clDtNow = QDateTime::currentDateTime();

   clDtNow   = QDateTime::currentDateTime();
   clDateNow = clDtNow.toLocalTime().date();
   iYear     = clDateNow.year();

   QDate       clDateBeginYear(iYear, 1, 1);
   clDtBeginYear.setDate(clDateBeginYear);
   //
   iSecs  = static_cast<int>(clDtBeginYear.secsTo(clDtNow));
   return(iSecs);

}

//
//  Function:  RTC_SecsSinceMidnight
//  Purpose:   Return last midnight in Secs
//
//  Parms:     
//  Returns:
//
int CPatrn::RTC_SecsSinceMidnight()
{
   int         iSecs, iYear, iMonth, iDay;
   QDate       clDateNow;
   QDateTime   clDtMidnight;
   QDateTime   clDtNow;
   QTimeZone   clTimeZone;

   clDtNow   = QDateTime::currentDateTime();
   clDateNow = clDtNow.toLocalTime().date();
   iYear     = clDateNow.year();
   iMonth    = clDateNow.month();
   iDay      = clDateNow.day();
   //
   QDate       clDateMidnight(iYear, iMonth, iDay);
   clDtMidnight.setDate(clDateMidnight);
   //
   iSecs = static_cast<int>(clDtMidnight.secsTo(clDtNow));
   return(iSecs);
}

//
//  Function:  RTC_SetDateDelimiter
//  Purpose:   Set the date string delimiter to default
//
//  Parms:     
//  Returns:
//
void CPatrn::RTC_SetDateDelimiter()
{
   cDateDelimiter = '-';
}

//
//  Function:  RTC_SetDateDelimiter
//  Purpose:   Set the date string delimiter
//
//  Parms:     Delimiter
//  Returns:
//
void CPatrn::RTC_SetDateDelimiter(char cDelim)
{
   cDateDelimiter = cDelim;
}


