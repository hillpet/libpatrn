/*  (c) Copyright:  2014..2020  Patrn ESS, Confidential Data
 *
 *  Workfile:           PatrnFiles.h
 *  Purpose:            Misc support functions
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:       QT5.10
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    15 Aug 2014: Created
 *    17 Apr 2020: Add RTC HH:MM
 *    20 Apr 2020: Add DST; Add various RTC functions
 *    26 Aug 2020: Add RTC_SecsToDate
 *    03 Mar 2023: Add LOG timestamp; Add GetInstances
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
#ifndef PATRN_FILES_H
#define PATRN_FILES_H

#include <QtCore>
#include <QDialog>
#include <QPlainTextEdit>
#include "libpatrn.h"
#include "RpiJson.h"

class CPatrn : public QDialog
{
    Q_OBJECT

public:
   explicit       CPatrn                  ();
   explicit       CPatrn                  (QString *);
                 ~CPatrn                  ();

   int            LIB_GetInstances        ();
   void           LIB_Init                ();
   //
   QString        LIB_BuildOptionsList    (CPiJson *);
   PTYPE          LIB_GetRootType         (CPiJson *, int);
   PTYPE          LIB_GetRootType         (CPiJson *, QString);
   PTYPE          LIB_GetRootType         (CPiJson *, const char *);
   //
   void           LOG_FileClose           ();
   void           LOG_FileFlush           ();
   bool           LOG_FileOpen            ();
   bool           LOG_FileOpen            (char);
   int            LOG_FileWrite           (QString *);
   int            LOG_FileWrite           (QString *, int);
   int            LOG_FileWrite           (char *);
   int            LOG_FileWrite           (char *, int);
   int            LOG_FileWrite           (const char *);
   int            LOG_FileWrite           (const char *, int);
   int            LOG_FileWrite           (const char *, QString);
   void           LOG_Timestamp           (bool);
   //
   void           MSG_AddText             (QPlainTextEdit *);
   void           MSG_AddText             (QPlainTextEdit *, QString);
   void           MSG_AddText             (QPlainTextEdit *, QString, QString);
   void           MSG_AddText             (QPlainTextEdit *, QString, qint64);
   void           MSG_AddText             (QPlainTextEdit *, QString, double);
   void           MSG_AddText             (QPlainTextEdit *, QString, int);
   void           MSG_Timestamp           (bool);
   bool           RTC_ConvertHms          (QString *, int *);
   bool           RTC_ConvertHm           (QString *, int *);
   QString        RTC_GetTimeDateFilename ();
   QString        RTC_GetTimeDateStamp    ();
   QString        RTC_GetDateStamp        ();
   QString        RTC_GetTimeStamp        ();
   QString        RTC_GetTimeStampShort   ();
   int            RTC_DateTimeToSecs      ();
   int            RTC_DateTimeToSecs      (QDate);
   void           RTC_SecsToDateTime      (QString *, int);
   void           RTC_SecsToDate          (QString *, int);
   void           RTC_SecsToDate          (int, int *, int *, int *);
   void           RTC_SecsToTime          (QString *, int);
   void           RTC_SecsToTime          (int, int *, int *, int *);
   //
   int            RTC_SecsUntilBeginYear  ();
   int            RTC_SecsSinceBeginYear  ();
   int            RTC_SecsSinceMidnight   ();
   //
   void           RTC_SetDateDelimiter    ();
   void           RTC_SetDateDelimiter    (char);
   //
   QString        RTC_ConvertToLdap       (int);
   int            RTC_ConvertFromLdap     (QString);

signals:

private slots:

private:
   static bool          fLogFileOpen;
   static int           iLogInstance;
   static QString      *pclLogName;
   static QTextStream  *pclLogStream;
   static QFile        *pclLogFile;
   //
   bool                 fMsgTimestamp;
   bool                 fLogTimestamp;
   char                 cDateDelimiter;
};

#endif // PATRN_FILES_H
