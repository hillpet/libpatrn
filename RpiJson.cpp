/*  (c) Copyright:      2014..2021 - Patrn, Confidential Data 
 *
 *  Workfile:           RpiJson.cpp
 *  Purpose:            JSON interface
 *                      
 *                      
 *  Compiler/Assembler: QT5.10-Platform independent
 *  Ext Packages:
 *  Note:               
 *
 *  Author:             Peter Hillen
 *  Changes:       
 *    26 Jul 2014:      Created
 *    19 Mar 2021:      Add PutRootKeyItem for int and bool
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

#include "libpatrn.h"
#include "RpiJson.h"

#if (QT_VERSION == QT_VERSION_CHECK(5,8,0))

//
// The global dirty var is static
// QT5.8: must declare both in *.H and *.cpp !!
// QT5.10.0 does not like this anymore ??
//
int CPiJson::iGlobalDirty;
#else

//
// The global dirty var is static
// QT5.4: CANNOT be declared both in *.H and *.cpp !!
//
#endif

//
//  Function:  CPiJson constructor
//  Purpose:   Construct RPi JSON class
//
//  Parms:     
//  Returns:   
//
CPiJson::CPiJson()
{
   iNumKeypairs      = 0;
   iJsonInt          = 0;
   iJsonArrIdx       = 0;
   iJsonArrElements  = 0;
   iLocalDirty       = 0;
   bJsonBool         = false;
   //
   // Use either the local Json document, or the global one
   // Start with the local Json document
   //
   pclGlobalDoc      = nullptr;
}

//
//  Function:  CPiJson destructor
//  Purpose:   Deconstruct RPi JSON class
//
//  Parms:     
//  Returns:   
//
CPiJson::~CPiJson()
{

}

/* ======   Local   Functions separator ===========================================
void _____PublicMethods_____(){}
==============================================================================*/

//
//  Function:  ClearAll
//  Purpose:   Clear all parameters
//
//  Parms:     Filename
//  Returns:   true if OK
//
void CPiJson::ClearAll()
{
   clStrList.clear();
   
   iNumKeypairs      = 0;
   iJsonInt          = 0;
   iJsonArrIdx       = 0;
   iJsonArrElements  = 0;
   bJsonBool         = false;
}


//
//  Function:  IsJsonObject
//  Purpose:   Quick check if the data is a JSON object
//
//  Parms:     Data
//  Returns:   true if OK
//
bool CPiJson::IsJsonObject(QByteArray baObj)
{
   bool  fCc=true;
   int   iCurOpen=0, iCurClose=0;
   int   iSqrOpen=0, iSqrClose=0;
   int   iLen;

   if(baObj[0] == '{')
   {
      iLen = baObj.length();
      //
      for(int i=0; i<iLen; i++)
      {
         switch(baObj[i])
         {
            case '{': iCurOpen++;   break;
            case '}': iCurClose++;  break;
            case '[': iSqrOpen++;   break;
            case ']': iSqrClose++;  break;
            default:                break;
         }
      }
      if(iSqrOpen != iSqrClose)  fCc = false;
      if(iCurOpen != iCurClose)  fCc = false;
   }
   else fCc = false;
   //
   return(fCc);
}

//
//  Function:  ReadDocumentFile
//  Purpose:   Read JsonDocument from file
//
//  Parms:     Filename
//  Returns:   ByteArray
//
QByteArray CPiJson::ReadDocumentFile(QString clCamParms)
{
   QByteArray  baParms;
   QFile       clFile(clCamParms);

   if( clFile.open(QIODevice::ReadOnly) )
   {
      baParms = clFile.readAll();
      clFile.close();
   }
   else
   {
      baParms  = "Unable to open CAM Json parameters ";
      baParms += clCamParms;
   }
   return(baParms);
}

//
//  Function:  WriteDocumentFile
//  Purpose:   Write JsonDocument to file
//
//  Parms:     Filename
//  Returns:   true if OK
//
bool CPiJson::WriteDocumentFile(QString clCamParms)
{
   bool           fCc=false;
   QFile          clFile(clCamParms);
   QJsonDocument  clDoc(clRootObj);

   if( clFile.open(QIODevice::WriteOnly) )
   {
      clFile.write(clDoc.toJson(QJsonDocument::Indented));
      clFile.close();
      fCc = true;
   }
   return(fCc);
}

//
//  Function:  ReadDocumentData
//  Purpose:   Read JsonDocument from byteArray
//
//             ****** Called by the DIALOG class ******
//
//  Parms:     ByteArray, JsonDoc, errordest
//  Returns:   true if OK
//  Note:      This function is used to parse any JSON object and if OK make it
//             available as a global top-level JSON Document
//
bool CPiJson::ReadDocumentData(QByteArray baJsonData, QJsonDocument *pclDoc, QString *pclError)
{
   bool              fCc=true;
   QString           clStr;
   QJsonParseError   clError;

   *pclDoc = QJsonDocument::fromJson(baJsonData, &clError);
   if(clError.error != QJsonParseError::NoError)
   {
      if(pclError)
      {
         *pclError = "Global ReadDocumentData Error:";
         *pclError += clError.errorString();
         clStr.sprintf(" (at offset %d)\n", clError.offset);
         *pclError += clStr;
      }
      fCc = false;
   }
   return(fCc);
}

//
//  Function:  ReadDocumentData
//
//             ****** Called by all TAB widgets ******
//
//  Purpose:   Read JsonDocument from byteArray
//
//  Parms:     ByteArray
//  Returns:   true if OK
//
bool CPiJson::ReadDocumentData(QByteArray baJsonData)
{
   return( ReadDocumentData(baJsonData, nullptr) );
}

//
//  Function:  ReadDocumentData
//  Purpose:   Read JsonDocument from byteArray
//
//             ****** Called by all TAB widgets ******
//
//  Parms:     ByteArray, Error string ptr
//  Returns:   true if OK
//  Note:      This function is used to parse any JSON object and if OK make it
//             available as a local top-level JSON Document
//
bool CPiJson::ReadDocumentData(QByteArray baJsonData, QString *pclError)
{
   bool              fCc=true;
   QString           clStr;
   QJsonParseError   clError;

   clParmDoc = QJsonDocument::fromJson(baJsonData, &clError);
   if(clError.error != QJsonParseError::NoError)
   {
      if(pclError)
      {
         *pclError = "Local ReadDocumentData Error:";
         *pclError += clError.errorString();
         clStr.sprintf(" (at offset %d)\n", clError.offset);
         *pclError += clStr;
      }
      fCc = false;
   }
   return(fCc);
}

//
//  Function:  SwitchToGlobalParameters
//  Purpose:   Start using the global top-level JSON document data that holds the global parameter list
//
//             ****** Called by all TAB widgets ******
//
//  Parms:     Master JSON doc ptr
//  Returns:   true if OKee
//  Note:      This function is called by all TAB widgets to enable the usage of the global
//             top-level JSON document. This should ensure all TAB widgets use the same set of JSON
//             parameters if they are being changed by the user in one particular TAB-widget.
//
bool CPiJson::SwitchToGlobalParameters(QJsonDocument *pclMasterDoc)
{
   pclGlobalDoc = pclMasterDoc;
   return(pclGlobalDoc != nullptr);
}

//
//  Function:  ParseLocalParameters
//  Purpose:   Extract all key-pairs from the JSON document
//
//  Parms:     
//  IN:        clParmDoc
//  OUT:       clRootObj, clStrList
//  Returns:   Number of key-pairs in the JSON document
//
//  Note:      This function is called to transfer a JSON document to the root object. After this, all
//             JSON methods can be used to parse the root-object for key-pairs.
//
//             QJsonDocument     ParseLocalParameters --> QJsonObject 
//             QJsonObject                            --> QStringList
//
int CPiJson::ParseLocalParameters()
{
   if( clParmDoc.isObject() )
   {
      clRootObj    = clParmDoc.object();
      iNumKeypairs = clRootObj.size();
      clStrList    = clRootObj.keys();
   }
   else
   {
      //
      // Document is not a JSON Object
      //
      iNumKeypairs = -1;
   }
   return(iNumKeypairs);
}

//
//  Function:  ParseGlobalParameters
//  Purpose:   Extract all key-pairs from the global JSON document
//
//  Parms:     
//  IN:        pclGlobalDoc
//  OUT:       clRootObj, clStrList
//  Returns:   Number of key-pairs in the JSON document
//
//  Note:      This function is called to transfer a JSON document to the root object. After this, all
//             JSON methods can be used to parse the root-object for key-pairs.
//
//             QJsonDocument     ParseLocalParameters --> QJsonObject 
//             QJsonObject                            --> QStringList
//
int CPiJson::ParseGlobalParameters()
{
   if( pclGlobalDoc && (pclGlobalDoc->isObject()) )
   {
      clRootObj    = pclGlobalDoc->object();
      iNumKeypairs = clRootObj.size();
      clStrList    = clRootObj.keys();
      iLocalDirty  = iGlobalDirty;
   }
   else
   {
      //
      // Document is not a JSON Object
      //
      iNumKeypairs = -1;
   }
   return(iNumKeypairs);
}

//
//  Function:  GetNumRootObjects
//  Purpose:   Return the number of keypairs in this root object
//
//  Parms:     
//  Returns:   number of root key-pairs
//
int CPiJson::GetNumRootObjects()
{
   return(iNumKeypairs);
}

//
//  Function:  SetObjectRoot
//  Purpose:   Set ROOT object
//
//  Parms:     
//  OUT:       clRootObj, clJsonObj
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::SetObjectRoot()
{
   CheckGlobalUpdate();
   //
   // Key is valid: 
   //    - reset the JSON object level
   //
   clJsonObj = clRootObj;
   
   return(JTYPE_UNDEFINED);
}

//
//  Function:  SetObjectRoot
//  Purpose:   Set ROOT object of the StringList[index] key-pair value
//
//  Parms:     Index
//  IN:        clStrList
//  OUT:       clRootObj, clJsonObj
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::SetObjectRoot(int iIdx)
{
   QString  clStr=GetObjectName(iIdx);

   return( SetObjectRoot(clStr) );
}

//
//  Function:  SetObjectRoot
//  Purpose:   Set ROOT object of this key-pair value
//
//  Parms:     Key
//  OUT:       clRootObj, clJsonObj
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::SetObjectRoot(QString clStr)
{
   JTYPE tType=JTYPE_UNDEFINED;

   if(clStr.length() > 0)
   {
      //
      // Key is valid: 
      //    - reset the JSON object level
      //    - retrieve the generic JSON value of the top-level object
      //
      SetObjectRoot();
      tType = ObjectToValue(clStr);
      ValueToStorage(tType);
   }
   return(tType);
}


/* ======   Local   Functions separator ===========================================
void _____GetObject_____(){}
==============================================================================*/

//
//  Function:  GetObjectName
//  Purpose:   Retrieve the root-level parameter name
//
//  Parms:     Index in the JSON root-object list
//  IN:        clStrList
//  Returns:   Key
//
QString CPiJson::GetObjectName(int iIdx)
{
   QString  clStr;

   if( (iNumKeypairs > 0) && (iIdx < iNumKeypairs) )
   {
      clStr = clStrList.at(iIdx);
   }
   return(clStr);
}

//
//  Function:  GetObjectIndex
//  Purpose:   Retrieve the root-level parameter index
//
//  Parms:     JSON root-object key
//  IN:        clStrList
//  Returns:   Index in the JSON root-object list
//
int CPiJson::GetObjectIndex(QString clName)
{
   return(clStrList.indexOf(clName));
}

//
//  Function:  GetObjectString
//  Purpose:   Get the QString for a JSON object
//
//  Parms:     Key1
//  Returns:   String
//
QString CPiJson::GetObjectString(const char *pcKey1)
{
   return(GetObjectString(pcKey1, (const char *)nullptr, (const char *)nullptr));
}

//
//  Function:  GetObjectString
//  Purpose:   Get the QString for a JSON object
//
//  Parms:     IndexKey1
//  Returns:   String
//
QString CPiJson::GetObjectString(int iKey1)
{
   return(GetObjectString(GetObjectKey(iKey1), (const char *)nullptr, (const char *)nullptr));
}

//
//  Function:  GetObjectString
//  Purpose:   Get the QString for a JSON object
//
//  Parms:     Key1, Arrayindex
//  Returns:   String
//
QString CPiJson::GetObjectString(const char *pcKey1, int iIdx)
{
   iJsonArrIdx = iIdx;
   return(GetObjectString(pcKey1, (const char *)nullptr, (const char *)nullptr));
}

//
//  Function:  GetObjectString
//  Purpose:   Get the QString for a JSON object
//
//  Parms:     IndexKey1, Arrayindex
//  Returns:   String
//
QString CPiJson::GetObjectString(int iKey1, int iIdx)
{
   iJsonArrIdx = iIdx;
   return(GetObjectString(GetObjectKey(iKey1), (const char *)nullptr, (const char *)nullptr));
}

//
//  Function:  GetObjectString
//  Purpose:   Get the QString for a JSON object
//
//  Parms:     Key1, Key2
//  Returns:   String
//
QString CPiJson::GetObjectString(const char *pcKey1, const char *pcKey2)
{
   return(GetObjectString(pcKey1, pcKey2, (const char *)nullptr));
}

//
//  Function:  GetObjectString
//  Purpose:   Get the QString for a JSON object
//
//  Parms:     IndexKey1, Key2
//  Returns:   String
//
QString CPiJson::GetObjectString(int iKey1, const char *pcKey2)
{
   return(GetObjectString(GetObjectKey(iKey1), pcKey2, (const char *)nullptr));
}

//
//  Function:  GetObjectString
//  Purpose:   Get the QString for a JSON object
//
//  Parms:     Key1, Key2, Arrayindex
//  Returns:   String
//
QString CPiJson::GetObjectString(const char *pcKey1, const char *pcKey2, int iIdx)
{
   iJsonArrIdx = iIdx;
   return(GetObjectString(pcKey1, pcKey2, (const char *)nullptr));
}

//
//  Function:  GetObjectString
//  Purpose:   Get the QString for a JSON object
//
//  Parms:     IndexKey1, Key2, Arrayindex
//  Returns:   String
//
QString CPiJson::GetObjectString(int iKey1, const char *pcKey2, int iIdx)
{
   iJsonArrIdx = iIdx;
   return(GetObjectString(GetObjectKey(iKey1), pcKey2, (const char *)nullptr));
}

//
//  Function:  GetObjectString
//  Purpose:   Get the QString for a JSON object
//
//  Parms:     IndexKey1, Key2, Key3
//  Returns:   String
//
QString CPiJson::GetObjectString(int iKey1, const char *pcKey2, const char *pcKey3)
{
   return(GetObjectString(GetObjectKey(iKey1), pcKey2, pcKey3));
}

//
//  Function:  GetObjectString
//  Purpose:   Get the QString for a JSON object
//
//  Parms:     IndexKey1, Key2, Key3, Arrayindex
//  Returns:   String
//
QString CPiJson::GetObjectString(int iKey1, const char *pcKey2, const char *pcKey3, int iIdx)
{
   iJsonArrIdx = iIdx;
   return(GetObjectString(GetObjectKey(iKey1), pcKey2, pcKey3));
}

//
//  Function:  GetObjectString
//  Purpose:   Get the QString for a JSON object
//
//  Parms:     Key1, Key2, Key3, Arrayindex
//  Returns:   String
//
QString CPiJson::GetObjectString(const char *pcKey1, const char *pcKey2, const char *pcKey3, int iIdx)
{
   iJsonArrIdx = iIdx;
   return(GetObjectString(pcKey1, pcKey2, pcKey3));
}

//
//  Function:  GetObjectString
//  Purpose:   Get the QString for a JSON object
//
//  Parms:     Key1, Key2, Key3
//  Returns:   String
//
QString CPiJson::GetObjectString(const char *pcKey1, const char *pcKey2, const char *pcKey3)
{
   char       *ppcKeys[4];

   ppcKeys[0] = (char *)pcKey1;
   ppcKeys[1] = (char *)pcKey2;
   ppcKeys[2] = (char *)pcKey3;
   ppcKeys[3] = nullptr;
   //
   return(GetStringValue(ppcKeys) );
}

//
//  Function:  GetObjectInteger
//  Purpose:   Get the integer for a JSON object
//
//  Parms:     Key1
//  Returns:   int
//
int CPiJson::GetObjectInteger(const char *pcKey1)
{
   return(GetObjectInteger(pcKey1, (const char *)nullptr, (const char *)nullptr));
}

//
//  Function:  GetObjectInteger
//  Purpose:   Get the integer for a JSON object
//
//  Parms:     IndexKey1
//  Returns:   int
//
int CPiJson::GetObjectInteger(int iKey1)
{
   return(GetObjectInteger(GetObjectKey(iKey1), (const char *)nullptr, (const char *)nullptr));
}

//
//  Function:  GetObjectInteger
//  Purpose:   Get the integer for a JSON object
//
//  Parms:     Key1, ArrayIndex
//  Returns:   int
//
int CPiJson::GetObjectInteger(const char *pcKey1, int iIdx)
{
   iJsonArrIdx = iIdx;
   return(GetObjectInteger(pcKey1, (const char *)nullptr, (const char *)nullptr));
}

//
//  Function:  GetObjectInteger
//  Purpose:   Get the integer for a JSON object
//
//  Parms:     IndexKey1, ArrayIndex
//  Returns:   int
//
int CPiJson::GetObjectInteger(int iKey1, int iIdx)
{
   iJsonArrIdx = iIdx;
   return(GetObjectInteger(GetObjectKey(iKey1), (const char *)nullptr, (const char *)nullptr));
}

//
//  Function:  GetObjectInteger
//  Purpose:   Get the integer for a JSON object
//
//  Parms:     Key1, Key2
//  Returns:   int
//
int CPiJson::GetObjectInteger(const char *pcKey1, const char *pcKey2)
{
   return(GetObjectInteger(pcKey1, pcKey2, (const char *)nullptr));
}

//
//  Function:  GetObjectInteger
//  Purpose:   Get the integer for a JSON object
//
//  Parms:     IndexKey1, Key2
//  Returns:   int
//
int CPiJson::GetObjectInteger(int iKey1, const char *pcKey2)
{
   char  *pcKey1;

   pcKey1 = GetObjectKey(iKey1);
   //
   return(GetObjectInteger((const char *)pcKey1, pcKey2, (const char *)nullptr));
}

//
//  Function:  GetObjectInteger
//  Purpose:   Get the integer for a JSON object
//
//  Parms:     Key1, Key2, ArrayIndex
//  Returns:   int
//
int CPiJson::GetObjectInteger(const char *pcKey1, const char *pcKey2, int iIdx)
{
   iJsonArrIdx = iIdx;
   return(GetObjectInteger(pcKey1, pcKey2, (const char *)nullptr));
}

//
//  Function:  GetObjectInteger
//  Purpose:   Get the integer for a JSON object
//
//  Parms:     IndexKey1, Key2, ArrayIndex
//  Returns:   int
//
int CPiJson::GetObjectInteger(int iKey1, const char *pcKey2, int iIdx)
{
   iJsonArrIdx = iIdx;
   return(GetObjectInteger(GetObjectKey(iKey1), pcKey2, (const char *)nullptr));
}

//
//  Function:  GetObjectInteger
//  Purpose:   Get the integer for a JSON object
//
//  Parms:     IndexKey1, Key2, Key3
//  Returns:   int
//
int CPiJson::GetObjectInteger(int iKey1, const char *pcKey2, const char *pcKey3)
{
   return(GetObjectInteger(GetObjectKey(iKey1), pcKey2, pcKey3));
}

//
//  Function:  GetObjectInteger
//  Purpose:   Get the integer for a JSON object
//
//  Parms:     Key1, Key2, Key3, ArrayIndex
//  Returns:   int
//
int CPiJson::GetObjectInteger(const char *pcKey1, const char *pcKey2, const char *pcKey3, int iIdx)
{
   iJsonArrIdx = iIdx;
   return(GetObjectInteger(pcKey1, pcKey2, pcKey3));
}

//
//  Function:  GetObjectInteger
//  Purpose:   Get the integer for a JSON object
//
//  Parms:     IndexKey1, Key2, Key3, ArrayIndex
//  Returns:   int
//
int CPiJson::GetObjectInteger(int iKey1, const char *pcKey2, const char *pcKey3, int iIdx)
{
   iJsonArrIdx = iIdx;
   return(GetObjectInteger(GetObjectKey(iKey1), pcKey2, pcKey3));
}

//
//  Function:  GetObjectInteger
//  Purpose:   Get the integer for a JSON object
//
//  Parms:     Key1, Key2, Key3
//  Returns:   int
//
int CPiJson::GetObjectInteger(const char *pcKey1, const char *pcKey2, const char *pcKey3)
{
   char       *ppcKeys[4];

   ppcKeys[0] = (char *)pcKey1;
   ppcKeys[1] = (char *)pcKey2;
   ppcKeys[2] = (char *)pcKey3;
   ppcKeys[3] = nullptr;
   //
   return(GetIntegerValue(ppcKeys) );
}

//
//  Function:  GetObjectType
//  Purpose:   Retrieve the type (and value) of this JSON object
//
//  Parms:     IndexKey1, Key2, Key3
//  Returns:   JTYPE_OBJECT
//
JTYPE CPiJson::GetObjectType(int iKey1, const char *pcKey2, const char *pcKey3)
{
   return( GetObjectType(GetObjectKey(iKey1), pcKey2, pcKey3) );
}

//
//  Function:  GetObjectType
//  Purpose:   Retrieve the type (and value) of this JSON object
//
//  Parms:     IndexKey1, Key2, Key3, ArrayIndex
//  Returns:   JTYPE_OBJECT
//
JTYPE CPiJson::GetObjectType(int iKey1, const char *pcKey2, const char *pcKey3, int iIdx)
{
   iJsonArrIdx = iIdx;
   return( GetObjectType(GetObjectKey(iKey1), pcKey2, pcKey3) );
}

//
//  Function:  GetObjectType
//  Purpose:   Retrieve the type (and value) of this JSON object
//
//  Parms:     Key1, Key2, Key3, Index
//  Returns:   JTYPE_OBJECT
//
JTYPE CPiJson::GetObjectType(const char *pcKey1, const char *pcKey2, const char *pcKey3, int iIdx)
{
   iJsonArrIdx = iIdx;
   return( GetObjectType(pcKey1, pcKey2, pcKey3) );
}

//
//  Function:  GetObjectType
//  Purpose:   Retrieve the type (and value) of this JSON object
//
//  Parms:     Key1, Key2, Key3
//  Returns:   JTYPE_OBJECT
//
JTYPE CPiJson::GetObjectType(const char *pcKey1, const char *pcKey2, const char *pcKey3)
{
   char *ppcKeys[4];

   ppcKeys[0] = (char *)pcKey1;
   ppcKeys[1] = (char *)pcKey2;
   ppcKeys[2] = (char *)pcKey3;
   ppcKeys[3] = nullptr;
   //
   return( StoreObjectValue(ppcKeys) );
}

//
//  Function:  GetObjectType
//  Purpose:   Retrieve the value of the local JSON object
//
//  Parms:     
//  Returns:   JTYPE_OBJECT, ...
//
JTYPE CPiJson::GetObjectType()
{
   if( clJsonVal.isObject() ) return(JTYPE_OBJECT);
   if( clJsonVal.isArray()  ) return(JTYPE_ARRAY);
   if( clJsonVal.isString() ) return(JTYPE_STRING);
   if( clJsonVal.isDouble() ) return(JTYPE_INTEGER);
   if( clJsonVal.isBool()   ) return(JTYPE_BOOL);
   if( clJsonVal.isNull()   ) return(JTYPE_NULL);
   //
   return(JTYPE_UNDEFINED);
}

//
//  Function:  GetObjectValue
//  Purpose:   Get the value for a JSON object
//
//  Parms:     Key1
//  Returns:   true if found
//
bool CPiJson::GetObjectValue(const char *pcKey1, int *piValue)
{
   bool  fCc=false;
   int   iIdx;

   iIdx = GetObjectIndex(pcKey1);
   if(iIdx >= 0)
   {
      switch( GetObjectType(pcKey1, (const char *)nullptr, (const char *)nullptr) )
      {
         default:
            break;

         case JTYPE_INTEGER:
            *piValue = GetObjectInteger(pcKey1, (const char *)nullptr, (const char *)nullptr);
            fCc = true;
      }
   }
   return(fCc);
}

//
//  Function:  StoreObjectValue
//  Purpose:   Convert the key-pair value and put it in local storage
//
//  Parms:     List of keynames (+nullptr)
//  Returns:   Final Type (should be JTYPE_STRING or JTYPE_INT
//
//  Note:      Retrieve value using clStr = GetValueString()
//                               or iVal  = GetValueInteger()
//                               or bVal  = GetValueBool()
//
JTYPE CPiJson::StoreObjectValue(const char **ppcTree)
{
   return( StoreObjectValue((char **) ppcTree) );
}

//
//  Function:  StoreObjectValue
//  Purpose:   Convert the key-pair value and put it in local storage
//
//  Parms:     List of keynames (+nullptr), ArrayIndex
//  Returns:   Final Type (should be JTYPE_STRING or JTYPE_INT
//
//  Note:      Retrieve value using clStr = GetValueString()
//                               or iVal  = GetValueInteger()
//                               or bVal  = GetValueBool()
//
JTYPE CPiJson::StoreObjectValue(const char **ppcTree, int iIdx)
{
   iJsonArrIdx = iIdx;
   return( StoreObjectValue((char **) ppcTree) );
}

//
//  Function:  StoreObjectValue
//  Purpose:   Convert the key-pair value and put it in local storage
//
//  Parms:     List of keynames (+nullptr), ArrayIndex
//  Returns:   Final Type (should be JTYPE_STRING or JTYPE_INT
//
//  Note:      Retrieve value using clStr = GetValueString()
//                               or iVal  = GetValueInteger()
//                               or bVal  = GetValueBool()
//
JTYPE CPiJson::StoreObjectValue(char **ppcTree, int iIdx)
{
   iJsonArrIdx = iIdx;
   return( StoreObjectValue(ppcTree) );
}

//
//  Function:  StoreObjectValue
//  Purpose:   Convert the key-pair value and put it in local storage
//
//  Parms:     List of keynames (+nullptr)
//  Returns:   Final Type (should be JTYPE_STRING or JTYPE_INT
//
//  Note:      Retrieve value using clStr = GetValueString()
//                               or iVal  = GetValueInteger()
//                               or bVal  = GetValueBool()
//
JTYPE CPiJson::StoreObjectValue(char **ppcTree)
{
   JTYPE tType=JTYPE_UNDEFINED;

   CheckGlobalUpdate();
   //
   // Restore Root Object
   //
   clJsonObj = clRootObj;
   //
   while(*ppcTree)
   {
      tType = ObjectToValue(*ppcTree);
      //
      // tType is the type of the key-pair's value
      //
      switch(tType)
      {
         default:
            return(JTYPE_UNDEFINED);

         case JTYPE_OBJECT:
            // clJsonObj=
            // 1: { "Key" : { "Subkey1" : value1 }
            // 2: { "Key" : { "Subkey1" : "value1" }
            //
            // Traverse to the next entry in the tree
            //
            ValueToStorage(tType);
            ppcTree++;
            break;

         case JTYPE_ARRAY:
            // clJsonArr=
            // 1: { "Key" : [ value1, value2 ] }
            // 2: { "Key" : [ "value1", "value2" ] }
            // 3: { "Key" : [ { "Subkey1" : value1, "Subkey2" : value2 } ] }
            // 4: { "Key" : [ { "Subkey1" : "value1", "Subkey2" : "value2" } ] }
            //
            // Check what type of array we have now:
            // 1--> Get value
            // 2--> Get "value"
            // 3--> New object
            //
            tType = GetArrayValue();
            ValueToStorage(tType);
            ppcTree++;
            break;

         case JTYPE_STRING:
         case JTYPE_INTEGER:
         case JTYPE_BOOL:
            //
            // We have reached the JSON value.
            // clJsonStr   contains the value as QString
            // clJsonInt   contains the value as Integer
            // clJsonBool  contains the value as bool
            // It should be the end of the tree as well !
            //
            ValueToStorage(tType);
            ppcTree++;
            break;
      }
   }
   //
   // We end up here, so we did not find a value at the end of the tree
   //
   return(tType);
}

/* ======   Local   Functions separator ===========================================
void _____PutObject_____(){}
==============================================================================*/

//
//  Function:  PutRootKey
//  Purpose:   Write to ROOT objects key-pair value
//
//  Parms:     IndexKey1, key2, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKey(int iKey1, QString clKey2, bool fValue)
{
   QString clKey1 = GetObjectName(iKey1);
   return( PutRootKey(clKey1, clKey2, fValue) );
}

//
//  Function:  PutRootKey
//  Purpose:   Write to ROOT objects key-pair value
//
//  Parms:     Key1, Key2, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKey(QString clKey1, QString clKey2, bool fValue)
{
   JTYPE tType=JTYPE_UNDEFINED;

   CheckGlobalUpdate();
   //
   clJsonVal = clRootObj[clKey1];
   tType     = GetObjectType();
   if(tType == JTYPE_OBJECT)
   {
      //
      // get the value
      //
      clJsonObj = clJsonVal.toObject();
      clJsonVal = clJsonObj[clKey2];
      tType     = GetObjectType();
      if(tType == JTYPE_BOOL)
      {
         clJsonObj[clKey2]= fValue;
         clRootObj[clKey1]= clJsonObj;
         DoGlobalUpdate();   // Mark global parameters have changed
      }
   }
   return(tType);
}

//
//  Function:  PutRootKey
//  Purpose:   Write to ROOT objects key-pair value
//
//  Parms:     IndexKey1, key2, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKey(int iKey1, QString clKey2, int iValue)
{
   QString clKey1 = GetObjectName(iKey1);
   return( PutRootKey(clKey1, clKey2, iValue) );
}

//
//  Function:  PutRootKey
//  Purpose:   Write to ROOT objects key-pair value
//
//  Parms:     Key1, key2, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKey(QString clKey1, QString clKey2, int iValue)
{
   JTYPE tType=JTYPE_UNDEFINED;

   CheckGlobalUpdate();
   //
   clJsonVal = clRootObj[clKey1];
   tType     = GetObjectType();
   if(tType == JTYPE_OBJECT)
   {
      //
      // get the value
      //
      clJsonObj = clJsonVal.toObject();
      clJsonVal = clJsonObj[clKey2];
      tType     = GetObjectType();
      if(tType == JTYPE_INTEGER)
      {
         clJsonObj[clKey2]= iValue;
         clRootObj[clKey1]= clJsonObj;
         DoGlobalUpdate();   // Mark global parameters have changed
      }
   }
   return(tType);
}

//
//  Function:  PutRootKey
//  Purpose:   Write to ROOT objects key-pair value
//
//  Parms:     IndexKey1, key2, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKey(int iKey1, QString clKey2, QString clValue)
{
   QString clKey1 = GetObjectName(iKey1);
   return( PutRootKey(clKey1, clKey2, clValue) );
}

//
//  Function:  PutRootKey
//  Purpose:   Write to ROOT objects key-pair value
//
//  Parms:     Key1, key2, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKey(QString clKey1, QString clKey2, QString clValue)
{
   JTYPE tType=JTYPE_UNDEFINED;

   CheckGlobalUpdate();
   //
   clJsonVal = clRootObj[clKey1];
   tType     = GetObjectType();
   if(tType == JTYPE_OBJECT)
   {
      //
      // get the value
      //
      clJsonObj = clJsonVal.toObject();
      clJsonVal = clJsonObj[clKey2];
      tType     = GetObjectType();
      if(tType == JTYPE_STRING)
      {
         clJsonObj[clKey2]= clValue;
         clRootObj[clKey1]= clJsonObj;
         DoGlobalUpdate();   // Mark global parameters have changed
      }
   }
   return(tType);
}

//
//  Function:  PutRootKeyItem
//  Purpose:   Write to ROOT objects key-pair value array item
//
//  Parms:     IndexKey1, key2, array-index, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKeyItem(int iKey1, QString clKey2, int iIdx, bool fValue)
{
   QString clKey1 = GetObjectName(iKey1);
   return( PutRootKeyItem(clKey1, clKey2, iIdx, fValue) );
}

//
//  Function:  PutRootKeyItem
//  Purpose:   Write to ROOT objects key-pair value array item
//
//  Parms:     Key1, key2, array-index, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKeyItem(QString clKey1, QString clKey2, int iIdx, bool fValue)
{
   int      iCount;
   JTYPE    tType=JTYPE_UNDEFINED;

   CheckGlobalUpdate();
   //
   clJsonVal = clRootObj[clKey1];
   tType     = GetObjectType();
   if(tType == JTYPE_OBJECT)
   {
      clJsonObj = clJsonVal.toObject();
      clJsonVal = clJsonObj[clKey2];
      tType     = GetObjectType();
      if(tType == JTYPE_ARRAY)
      {
         clJsonArr = clJsonVal.toArray();
         iCount = clJsonArr.count();
         if(iIdx < iCount)
         {
            clJsonVal = clJsonArr[iIdx];
            tType     = GetObjectType();
            if(tType == JTYPE_BOOL)
            {
               clJsonArr[iIdx]  = fValue;
               clJsonObj[clKey2]= clJsonArr;
               clRootObj[clKey1]= clJsonObj;
               DoGlobalUpdate();   // Mark global parameters have changed
            }
         }
      }
   }
   return(tType);
}

//
//  Function:  PutRootKeyItem
//  Purpose:   Write to ROOT objects key-pair value array item
//
//  Parms:     IndexKey1, key2, array-index, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKeyItem(int iKey1, QString clKey2, int iIdx, int iValue)
{
   QString clKey1 = GetObjectName(iKey1);
   return( PutRootKeyItem(clKey1, clKey2, iIdx, iValue) );
}

//
//  Function:  PutRootKeyItem
//  Purpose:   Write to ROOT objects key-pair value array item
//
//  Parms:     Key1, key2, array-index, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKeyItem(QString clKey1, QString clKey2, int iIdx, int iValue)
{
   int      iCount;
   JTYPE    tType=JTYPE_UNDEFINED;

   CheckGlobalUpdate();
   //
   clJsonVal = clRootObj[clKey1];
   tType     = GetObjectType();
   if(tType == JTYPE_OBJECT)
   {
      clJsonObj = clJsonVal.toObject();
      clJsonVal = clJsonObj[clKey2];
      tType     = GetObjectType();
      if(tType == JTYPE_ARRAY)
      {
         clJsonArr = clJsonVal.toArray();
         iCount = clJsonArr.count();
         if(iIdx < iCount)
         {
            clJsonVal = clJsonArr[iIdx];
            tType     = GetObjectType();
            if(tType == JTYPE_INTEGER)
            {
               clJsonArr[iIdx]  = iValue;
               clJsonObj[clKey2]= clJsonArr;
               clRootObj[clKey1]= clJsonObj;
               DoGlobalUpdate();   // Mark global parameters have changed
            }
         }
      }
   }
   return(tType);
}

//
//  Function:  PutRootKeyItem
//  Purpose:   Write to ROOT objects key-pair value array item
//
//  Parms:     IndexKey1, key2, array-index, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKeyItem(int iKey1, QString clKey2, int iIdx, QString clValue)
{
   QString clKey1 = GetObjectName(iKey1);
   return( PutRootKeyItem(clKey1, clKey2, iIdx, clValue) );
}

//
//  Function:  PutRootKeyItem
//  Purpose:   Write to ROOT objects key-pair value array item
//
//  Parms:     Key1, key2, array-index, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKeyItem(QString clKey1, QString clKey2, int iIdx, QString clValue)
{
   int      iCount;
   JTYPE    tType=JTYPE_UNDEFINED;

   CheckGlobalUpdate();
   //
   clJsonVal = clRootObj[clKey1];
   tType     = GetObjectType();
   if(tType == JTYPE_OBJECT)
   {
      clJsonObj = clJsonVal.toObject();
      clJsonVal = clJsonObj[clKey2];
      tType     = GetObjectType();
      if(tType == JTYPE_ARRAY)
      {
         clJsonArr = clJsonVal.toArray();
         iCount = clJsonArr.count();
         if(iIdx < iCount)
         {
            clJsonVal = clJsonArr[iIdx];
            tType     = GetObjectType();
            if(tType == JTYPE_STRING)
            {
               clJsonArr[iIdx]  = clValue;
               clJsonObj[clKey2]= clJsonArr;
               clRootObj[clKey1]= clJsonObj;
               DoGlobalUpdate();   // Mark global parameters have changed
            }
         }
      }
   }
   return(tType);
}

//
//  Function:  PutRootKeyItem
//  Purpose:   Write to ROOT objects key-pair value array item
//
//  Parms:     Key1, key2, array-index, SubKey, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKeyItem(QString clKey1, QString clKey2, int iIdx, QString clSubKey, QString clValue)
{
   int         iCount;
   QJsonObject clObj;
   JTYPE       tType=JTYPE_UNDEFINED;

   CheckGlobalUpdate();
   //
   clJsonVal = clRootObj[clKey1];
   tType     = GetObjectType();
   if(tType == JTYPE_OBJECT)
   {
      clJsonObj = clJsonVal.toObject();
      clJsonVal = clJsonObj[clKey2];
      tType     = GetObjectType();
      if(tType == JTYPE_ARRAY)
      {
         clJsonArr = clJsonVal.toArray();
         iCount = clJsonArr.count();
         if(iIdx < iCount)
         {
            clJsonVal = clJsonArr[iIdx];
            tType     = GetObjectType();
            if(tType == JTYPE_OBJECT)
            {
               clObj     = clJsonVal.toObject();
               clJsonVal = clObj[clSubKey];
               tType     = GetObjectType();
               if(tType == JTYPE_STRING)
               {
                  clObj[clSubKey]  = clValue;
                  clJsonArr[iIdx]  = clObj;
                  clJsonObj[clKey2]= clJsonArr;
                  clRootObj[clKey1]= clJsonObj;
                  DoGlobalUpdate();   // Mark global parameters have changed
               }
            }
         }
      }
   }
   return(tType);
}

//
//  Function:  PutRootKeyItem
//  Purpose:   Write to ROOT objects key-pair value array item
//
//  Parms:     Key1, key2, array-index, SubKey, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKeyItem(QString clKey1, QString clKey2, int iIdx, QString clSubKey, int iValue)
{
   int         iCount;
   QJsonObject clObj;
   JTYPE       tType=JTYPE_UNDEFINED;

   CheckGlobalUpdate();
   //
   clJsonVal = clRootObj[clKey1];
   tType     = GetObjectType();
   if(tType == JTYPE_OBJECT)
   {
      clJsonObj = clJsonVal.toObject();
      clJsonVal = clJsonObj[clKey2];
      tType     = GetObjectType();
      if(tType == JTYPE_ARRAY)
      {
         clJsonArr = clJsonVal.toArray();
         iCount = clJsonArr.count();
         if(iIdx < iCount)
         {
            clJsonVal = clJsonArr[iIdx];
            tType     = GetObjectType();
            if(tType == JTYPE_OBJECT)
            {
               clObj     = clJsonVal.toObject();
               clJsonVal = clObj[clSubKey];
               tType     = GetObjectType();
               if(tType == JTYPE_INTEGER)
               {
                  clObj[clSubKey]  = iValue;
                  clJsonArr[iIdx]  = clObj;
                  clJsonObj[clKey2]= clJsonArr;
                  clRootObj[clKey1]= clJsonObj;
                  DoGlobalUpdate();   // Mark global parameters have changed
               }
            }
         }
      }
   }
   return(tType);
}

//
//  Function:  PutRootKeyItem
//  Purpose:   Write to ROOT objects key-pair value array item
//
//  Parms:     Key1, key2, array-index, SubKey, value
//  IN:        clRootObj
//  OUT:       clRootObj, clJsonVal
//  Returns:   JSON Type (JTYPE_xxx)
//
JTYPE CPiJson::PutRootKeyItem(QString clKey1, QString clKey2, int iIdx, QString clSubKey, bool fValue)
{
   int         iCount;
   QJsonObject clObj;
   JTYPE       tType=JTYPE_UNDEFINED;

   CheckGlobalUpdate();
   //
   clJsonVal = clRootObj[clKey1];
   tType     = GetObjectType();
   if(tType == JTYPE_OBJECT)
   {
      clJsonObj = clJsonVal.toObject();
      clJsonVal = clJsonObj[clKey2];
      tType     = GetObjectType();
      if(tType == JTYPE_ARRAY)
      {
         clJsonArr = clJsonVal.toArray();
         iCount = clJsonArr.count();
         if(iIdx < iCount)
         {
            clJsonVal = clJsonArr[iIdx];
            tType     = GetObjectType();
            if(tType == JTYPE_OBJECT)
            {
               clObj     = clJsonVal.toObject();
               clJsonVal = clObj[clSubKey];
               tType     = GetObjectType();
               if(tType == JTYPE_INTEGER)
               {
                  clObj[clSubKey]  = fValue;
                  clJsonArr[iIdx]  = clObj;
                  clJsonObj[clKey2]= clJsonArr;
                  clRootObj[clKey1]= clJsonObj;
                  DoGlobalUpdate();   // Mark global parameters have changed
               }
            }
         }
      }
   }
   return(tType);
}

/* ======   Local   Functions separator ===========================================
void _____Local_Storage_____(){}
==============================================================================*/

//
//  Function:  GetArrayNumElements
//  Purpose:   Retrieve the number of elements in the last array
//
//  Parms:     
//  Returns:   JTYPE
//
int CPiJson::GetArrayNumElements()
{
   return(iJsonArrElements);
}

//
//  Function:  GetValueBool
//  Purpose:   Retrieve the key-pair value : bool
//
//  Parms:     
//  Returns:   bJsonBool
//
bool CPiJson::GetValueBool()
{
   return(bJsonBool);
}

//
//  Function:  GetValueInteger
//  Purpose:   Retrieve the key-pair value : int
//
//  Parms:     
//  Returns:   iJsonInt
//
int CPiJson::GetValueInteger()
{
   return(iJsonInt);
}

//
//  Function:  GetValueString
//  Purpose:   Retrieve the key-pair value : QString
//
//  Parms:     
//  Returns:   clJsonStr
//
QString CPiJson::GetValueString()
{
   return(clJsonStr);
}

/* ======   Local   Functions separator ===========================================
void _____Private_Methods_____(){}
==============================================================================*/

//
//  Function:  CheckGlobalUpdate
//  Purpose:   Check if the global parameters have been updated for this instance
//
//  Parms:     
//  IN:        iGlobalDirty
//  IN:        iLocalDirty
//  Returns:   true if global JSON Doc was updated.
//
bool CPiJson::CheckGlobalUpdate()
{
   bool fCc=false;

   if(pclGlobalDoc)
   {
      //
      // This instance does use the global parameter list
      //    o Update the global json document
      //
      if(iGlobalDirty != iLocalDirty)
      {
         //
         // There has been a global JSON Doc update: 
         //    o reparse these into our local settings
         //
         ParseGlobalParameters();
         fCc = true;
      }
   }
   return(fCc);
}

//
//  Function:  DoGlobalUpdate
//  Purpose:   Update the document holding the global parameters
//
//  Parms:     
//  Returns:   true if global JSON Doc was updated.
//
bool CPiJson::DoGlobalUpdate()
{
   bool fCc=false;

   if(pclGlobalDoc)
   {
      QJsonDocument  clDoc(clRootObj);

      //
      // There has been a global JSON Doc update: 
      //    o reparse these into our local settings
      //
      *pclGlobalDoc = clDoc;
      iGlobalDirty++;
      fCc = true;
   }
   return(fCc);
}

//
//  Function:  GetObjectKey
//  Purpose:   Retrieve the root-level parameter name
//
//  Parms:     Index in the list
//  IN:        clStrList
//  Returns:   Key
//  Note:      During the parsing of the JSON object, the list of root key-pair names
//             has been store in a local list. Use this to retrieve the desired name.
//
char *CPiJson::GetObjectKey(int iIdx)
{
   QString clStr;

   clStr = GetObjectName(iIdx);
   //
   // Translate to JSON key
   //
   baJsonText = clStr.toLocal8Bit();
   //
   return(baJsonText.data());
}

//
//  Function:  GetStringValue
//  Purpose:   Get the QString for a parameter
//
//  Parms:     Key list (+nullptr)
//  Returns:   String
//
QString CPiJson::GetStringValue(char **ppcKeys)
{
   int      iValue=0;
   QString  clStr;

   //
   // Retrieve the JSON value
   //
   switch( StoreObjectValue(ppcKeys) )
   {
      default:
         break;

      case JTYPE_BOOL:
         if(GetValueBool() == true) clStr = "true";
         else                       clStr = "false";
         break;

      case JTYPE_INTEGER:
         iValue = GetValueInteger();
         clStr.sprintf("%d", iValue);
         break;

      case JTYPE_STRING:
         clStr = GetValueString();
         break;
   }
   return(clStr);
}

//
//  Function:  GetIntegerValue
//  Purpose:   Get an integer from a list of JSON objects
//
//  Parms:     Key list (+nullptr)
//  Returns:   int
//
int CPiJson::GetIntegerValue(char **ppcKeys)
{
   QString  clStr;
   int      iValue=0;

   //
   // Retrieve the JSON value
   //
   switch( StoreObjectValue(ppcKeys) )
   {
      default:
         break;

      case JTYPE_BOOL:
         if(GetValueBool() == true) iValue = true;
         else                       iValue = false;
         break;

      case JTYPE_STRING:
         clStr  = GetValueString();
         iValue = clStr.toInt();
         break;

      case JTYPE_INTEGER:
         iValue = GetValueInteger();
         break;
   }
   return(iValue);
}

//
//  Function:  GetArrayValue
//  Purpose:   Retrieve the key-pair value : array
//
//  Parms:     
//  IN:        clJsonVal
//  OUT:       iJsonArrElements
//  OUT:       clJsonArr
//  OUT:       clJsonVal
//  Returns:   JTYPE
//
JTYPE CPiJson::GetArrayValue()
{
   JTYPE tType;

   CheckGlobalUpdate();
   //
   clJsonArr         = clJsonVal.toArray();
   iJsonArrElements  = clJsonArr.count();
   clJsonVal         = clJsonArr.at(iJsonArrIdx);
   tType             = GetObjectType();
   //
   return(tType);
}

//
//  Function:  ObjectToValue
//  Purpose:   Retrieve the value of the current JSON object
//
//  Parms:     Key
//  IN:        clJsonObj
//  OUT:       clJsonVAL
//  Returns:   JTYPE_OBJECT, ...
//
JTYPE CPiJson::ObjectToValue(QString clStr)
{
   JTYPE tType=JTYPE_UNDEFINED;

   CheckGlobalUpdate();
   //
   if(clStr.length() > 0)
   {
      //
      // Key is valid: get the value
      //
      clJsonVal = clJsonObj.value(clStr);
      tType     = GetObjectType();
   }
   return(tType);
}

//
//  Function:  ValueToStorage
//  Purpose:   Set the local storage to the key-pair value
//
//  Parms:     JSON Type
//  IN:        clJsonVAL
//  Returns:   True if value is OKee
//
bool CPiJson::ValueToStorage(JTYPE tType)
{
   bool fCc=true;

   CheckGlobalUpdate();
   //
   // Set the local JSON object to the type of this <"Key":Value>
   //
   //    tType:      Changes:
   // ----------------------------------------------------------
   //    OBJECT      clJsonObj
   //    ARRAY       clJsonArr
   //    STRING      clJsonStr
   //    INTEGER     iJsonInt
   //    BOOL        bJsonBool
   //
   switch(tType)
   {
      default:
         fCc=false;
         break;

      case JTYPE_ARRAY:
         clJsonArr = clJsonVal.toArray();
         break;

      case JTYPE_BOOL:
         bJsonBool = clJsonVal.toBool();
         break;

      case JTYPE_INTEGER:
         iJsonInt = clJsonVal.toDouble();
         break;

      case JTYPE_OBJECT:
         clJsonObj = clJsonVal.toObject();
         break;

      case JTYPE_STRING:
         clJsonStr = clJsonVal.toString();
         break;
   }
   return(fCc);
}

