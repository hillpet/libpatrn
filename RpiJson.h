/*  (c) Copyright:      2014..2021 - Patrn, Confidential Data 
 *
 *  Workfile:           RpiJson.h
 *  Purpose:            JSON interface
 *                      
 *                      
 *  Compiler/Assembler: QT5.10-Platform independent
 *  Ext Packages:
 *  Note:               
 *
 *  Author:             Peter Hillen
 *  Changes:       
 *    26 Jul 2014:      Created
 *    19 Mar 2021:      Add PutRootKeyItem for int and bool
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

#ifndef RPIJSON_H
#define RPIJSON_H

#include <QtGlobal>
#include <QDialog>
#include <QFile>
#include <QByteArray>
#include <QStringList>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonParseError>


typedef enum  _JTYPE_
{
   JTYPE_UNDEFINED = 0,
   JTYPE_NULL,
   JTYPE_ARRAY,
   JTYPE_BOOL,
   JTYPE_INTEGER,
   JTYPE_OBJECT,
   JTYPE_STRING
}  JTYPE;

typedef enum  _PTYPE_
{
   PTYPE_UNDEFINED = 0,
   PTYPE_ENUM,
   PTYPE_OBJECT,
   PTYPE_BOOL,
   PTYPE_INTEGER,
   PTYPE_NUMBER,
   PTYPE_STRING
}  PTYPE;

//
// JSON Class
//
class CPiJson
{
public:
                  CPiJson                 ();
   virtual       ~CPiJson                 ();
   //
   void           ClearAll                ();
   bool           IsJsonObject            (QByteArray);
   QByteArray     ReadDocumentFile        (QString);
   bool           WriteDocumentFile       (QString);
   bool           ReadDocumentData        (QByteArray);
   bool           ReadDocumentData        (QByteArray, QString *);
   bool           ReadDocumentData        (QByteArray, QJsonDocument *, QString *);
   bool           SwitchToGlobalParameters(QJsonDocument *);
   int            ParseGlobalParameters   ();
   int            ParseLocalParameters    ();
   JTYPE          SetObjectRoot           ();
   JTYPE          SetObjectRoot           (int);
   JTYPE          SetObjectRoot           (QString);
   //
   QString        GetObjectName           (int);
   int            GetObjectIndex          (QString);
   //
   QString        GetObjectString         (const char *);
   QString        GetObjectString         (int);
   QString        GetObjectString         (const char *, int);
   QString        GetObjectString         (int, int);
   //
   QString        GetObjectString         (const char *, const char *);
   QString        GetObjectString         (int, const char *);
   QString        GetObjectString         (const char *, const char *, int);
   QString        GetObjectString         (int, const char *, int);
   //
   QString        GetObjectString         (const char *, const char *, const char *);
   QString        GetObjectString         (int, const char *, const char *);
   QString        GetObjectString         (const char *, const char *, const char *, int);
   QString        GetObjectString         (int, const char *, const char *, int);
   //
   int            GetObjectInteger        (const char *);
   int            GetObjectInteger        (int);
   int            GetObjectInteger        (const char *, int);
   int            GetObjectInteger        (int, int);
   //
   int            GetObjectInteger        (const char *, const char *);
   int            GetObjectInteger        (int, const char *);
   int            GetObjectInteger        (const char *, const char *, int);
   int            GetObjectInteger        (int, const char *, int);
   //
   int            GetObjectInteger        (const char *, const char *, const char *);
   int            GetObjectInteger        (int, const char *, const char *);
   int            GetObjectInteger        (const char *, const char *, const char *, int);
   int            GetObjectInteger        (int, const char *, const char *, int);
   //
   JTYPE          GetObjectType           (int, const char *, const char *);
   JTYPE          GetObjectType           (int, const char *, const char *, int);
   JTYPE          GetObjectType           (const char *, const char *, const char *, int);
   JTYPE          GetObjectType           (const char *, const char *, const char *);
   JTYPE          GetObjectType           ();
   //
   bool           GetObjectValue          (const char *, int *);
   //
   JTYPE          StoreObjectValue        (const char **, int);
   JTYPE          StoreObjectValue        (const char **);
   JTYPE          StoreObjectValue        (char **, int);
   JTYPE          StoreObjectValue        (char **);
   //
   JTYPE          PutRootKey              (int, QString, bool);
   JTYPE          PutRootKey              (QString, QString, bool);
   JTYPE          PutRootKey              (int, QString, int);
   JTYPE          PutRootKey              (QString, QString, int);
   JTYPE          PutRootKey              (int, QString, QString);
   JTYPE          PutRootKey              (QString, QString, QString);
   //
   JTYPE          PutRootKeyItem          (int, QString, int, bool);
   JTYPE          PutRootKeyItem          (QString, QString, int, bool);
   JTYPE          PutRootKeyItem          (int, QString, int, int);
   JTYPE          PutRootKeyItem          (QString, QString, int, int);
   JTYPE          PutRootKeyItem          (int, QString, int, QString);
   JTYPE          PutRootKeyItem          (QString, QString, int, QString);
   JTYPE          PutRootKeyItem          (QString, QString, int, QString, QString);
   JTYPE          PutRootKeyItem          (QString, QString, int, QString, int);
   JTYPE          PutRootKeyItem          (QString, QString, int, QString, bool);
   //
   int            GetArrayNumElements     ();
   int            GetNumRootObjects       ();
   bool           GetValueBool            ();
   int            GetValueInteger         ();
   QString        GetValueString          ();
   //
private slots:

signals:

private:
   bool           CheckGlobalUpdate       ();
   bool           DoGlobalUpdate          ();
   //
   char          *GetObjectKey            (int);
   JTYPE          GetArrayValue           ();
   int            GetIntegerValue         (char **);
   QString        GetStringValue          (char **);
   JTYPE          ObjectToValue           (QString);
   bool           ValueToStorage          (JTYPE);
   //
   // The global dirty var is static
   // QT5.8: must declare in *.cpp as well !
   //
   static int     iGlobalDirty;
   //
   int            iLocalDirty;
   int            iNumKeypairs;
   //
   QJsonDocument *pclGlobalDoc;           // Parameter Global JSON Document
   QJsonDocument  clParmDoc;              // Parameter Local  JSON Document
   //
   QStringList    clStrList;              // Parameter list
   QJsonObject    clRootObj;              // Root JSON OBJECT
   //
   // Local JSON Value storage:
   //
   QJsonValue     clJsonVal;
   //
   // FILE              ReadDocumentFile     --> QByteArray
   // CLOUD             HTTP Reply           --> QByteArray
   //
   // QByteArray        ReadDocumentData     --> QJsonDocument
   // QJsonDocument     ParseLocalParameters --> QJsonObject 
   // QJsonObject                            --> QStringList
   //
   // QStringList       GetObjectKey         --> char *
   // QStringList       GetObjectName        --> QString
   //
   bool           bJsonBool;
   int            iJsonInt;
   int            iJsonArrIdx;
   int            iJsonArrElements;
   QString        clJsonStr;
   QJsonObject    clJsonObj;
   QJsonArray     clJsonArr;
   QByteArray     baJsonText;
};

#endif // RPIJSON_H
