/*  (c) Copyright:  2014 PATRN.NL
**
**  $Workfile:  libpatrn.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    
**
**  Entries:
**
**
**
**
**
 *  Compiler/Assembler: QT5-Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       24 Feb 2014
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _LIBPATRN_H_
#define _LIBPATRN_H_

#include <QtCore/QtGlobal>

#if defined(LIBPATRN_LIBRARY)
#define LIBPATRN_EXPORT Q_DECL_EXPORT
#else
#define LIBPATRN_EXPORT Q_DECL_IMPORT
#endif


#endif // _LIBPATRN_H_
