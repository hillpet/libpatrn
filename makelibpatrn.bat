@echo off
rem
rem QT5 LibPatrn builder helper: save new libpatrn.a to the correct location
rem

echo QT5 LibPatrn builder helper: Save new libpatrn.a to the correct location
echo %date%--%time% %1,%2 >>libpatrn.log

if .%2.==.STATICRELEASE. goto staticrelease
if .%2.==.DYNAMICRELEASE. goto dynamicrelease
if .%2.==.DYNAMICDEBUG. goto dynamicdebug

echo Incorrect action %2 >>libpatrn.log
exit 255


:staticrelease
if not exist %1\release\libpatrn.a goto nosuchlib
echo copy %1\release\libpatrn.a %1\..\Libs\libpatrn.static.release.a >>libpatrn.log
copy %1\release\libpatrn.a %1\..\Libs\libpatrn.static.release.a
goto end

:dynamicrelease
if not exist %1\release\libpatrn.a goto nosuchlib
echo copy %1\release\libpatrn.a %1\..\Libs\libpatrn.dynamic.release.a >>libpatrn.log
copy %1\release\libpatrn.a %1\..\Libs\libpatrn.dynamic.release.a
goto end

:dynamicdebug
if not exist %1\debug\libpatrn.a goto nosuchlib
echo copy %1\debug\libpatrn.a %1\..\Libs\libpatrn.dynamic.debug.a >>libpatrn.log
copy %1\debug\libpatrn.a %1\..\Libs\libpatrn.dynamic.debug.a
goto end

:nosuchlib
echo ERROR: %1\xxxx\libpatrn.a not found ! >>libpatrn.log
exit 255

:end
echo copy %1\..\LibPatrn\*.h %1\..\Include\*.h >>libpatrn.log
copy %1\..\LibPatrn\*.h %1\..\Include\*.h
